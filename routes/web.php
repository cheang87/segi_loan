<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $myr_value = segiAPICall();

    return view('index',['result' => $myr_value]);
});

Route::get('/phpinfo', function () {
    phpinfo(); 
});

Route::get('/conversion','App\Http\Controllers\ConversionController@index')->name('segi.price');
