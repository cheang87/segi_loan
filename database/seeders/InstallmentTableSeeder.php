<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Installment;

class InstallmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Installment::factory()->count(20)->create();
    }
}
