<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Category;

class ApplicationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "new" => $this->faker->numberBetween(0,1),
            "online_commerce" => $this->faker->numberBetween(0,1),
            "loanee_Id" => $this->faker->phoneNumber(),
            "loanee_name" => $this->faker->name(),
            'loanee_company_registration_no' => $this->faker->text(),
            "loanee_company" => $this->faker->company(),
            "loanee_company_address" => $this->faker->address(),
            "loanee_company_state" => $this->faker->state(),
            "loanee_company_contact" => $this->faker->phoneNumber(),
            "loanee_email" => $this->faker->safeEmail(),
            "category_id" => $this->faker->numberBetween(1,Category::count()),
            "loan_officer_name" => $this->faker->name() ,
            "loan_officer_contact" => $this->faker->phoneNumber(),
            "loan_officer_email" => $this->faker->safeEmail(),
            "loan_amount" => $this->faker->numberBetween(1000,10000),
            "loan_period" => $this->faker->numberBetween(0,Category::count()),
            "loan_status" => $this->faker->numberBetween(0,2),
            "file_url" => $this->faker->imageUrl($width = 200, $height = 200)
        ];
    }
}
