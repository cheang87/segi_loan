<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Application;

class InstallmentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "application_id" => $this->faker->numberBetween(1,Application::count()),
            "payable_date" => $this->faker->date(),
            "total_paid_months" => 1,
            "amount" => $this->faker->numberBetween(100,1000),
            "file_url" => $this->faker->imageUrl($width = 200, $height = 200),
        ];
    }
}
