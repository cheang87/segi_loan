<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->id();
            $table->integer('new')->comment('0=new , 1=existed');
            $table->integer('online_commerce')->comment('0=no , 1=yes');
            $table->string('loanee_Id');
            $table->string('loanee_name');
            $table->string('loanee_company');
            $table->string('loanee_company_address');
            $table->string('loanee_company_state');
            $table->string('loanee_company_contact');
            $table->string('loanee_email');
            $table->foreignId('category_id')->constrained();
            $table->string('loan_officer_name');
            $table->string('loan_officer_contact');
            $table->string('loan_officer_email');
            $table->decimal('loan_amount', 7,2);
            $table->integer('loan_period');
            $table->integer('loan_status')->comment('0=freeze,1=in_progress,2=completed');
            $table->string('file_url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
