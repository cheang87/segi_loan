<!DOCTYPE html>
<html lang="en-US" class="no-js" itemscope itemtype="https://schema.org/WebPage">

<head>
    <meta charset="UTF-8" />
    <link rel="alternate" hreflang="en-US" href="{{ url()->current(); }}" />
    <title>SegiCoin Group - 我们是一间马来西亚拥有良好信誉的数码货币贷款公司.本公司已经拥有多年的营业经验。</title>
    <meta name='robots' content='max-image-preview:large' />
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="shortcut icon" href='{{ asset("images/segi.ico") }}' type="image/x-icon" />
    <link rel='dns-prefetch' href='//fonts.googleapis.com' />
    <link rel='dns-prefetch' href='//s.w.org' />
    <link rel="canonical" href="{{ url()->current(); }}" />
    <link rel='shortlink' href='{{ url()->current(); }}' />

    <link rel='stylesheet' id='mfn-fonts-css'
        href='https://fonts.googleapis.com/css?family=DM+Sans%3A1%2C300%2C400%2C400italic%2C500%2C700%2C700italic&#038;display=swap&#038;ver=5.8.2'
        type='text/css' media='all' />
    <link href="https://fonts.googleapis.com/css2?family=Overpass:wght@100;400;500;700&display=swap" rel="stylesheet">
    <link rel='stylesheet' id='mfn-font-button-css'
        href='https://fonts.googleapis.com/css?family=DM+Sans%3A400%2C700&#038;display=swap&#038;ver=5.8.2'
        type='text/css' media='all' />
    <link rel='stylesheet' id='google-fonts-1-css'
        href='https://fonts.googleapis.com/css?family=Roboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto+Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;display=auto&#038;ver=5.8.2'
        type='text/css' media='all' />
    <link href="https://fonts.googleapis.com/css?family=DM+Sans:500%2C700%7CRoboto:400" rel="stylesheet"
        property="stylesheet" media="all" type="text/css">

    <link rel='stylesheet' id='wp-block-library-css' href='{{ asset("css/style.min.css") }}' type='text/css'
        media='all' />
    <link rel='stylesheet' id='dashicons-css' href='{{ asset("css/dashicons.min.css") }}' type='text/css' media='all' />
    <link rel='stylesheet' id='everest-forms-general-css' href='{{ asset("css/everest-forms.css") }}' type='text/css'
        media='all' />
    <link rel='stylesheet' id='mfn-be-css' href=' {{ asset("css/be.css") }}' type='text/css' media='all' />
    <link rel='stylesheet' id='mfn-animations-css' href='{{ asset("css/animations.min.css") }}' type='text/css'
        media='all' />
    <link rel='stylesheet' id='mfn-font-awesome-css' href='{{ asset("css/fontawesome.css") }}' type='text/css'
        media='all' />
    <link rel='stylesheet' id='mfn-jplayer-css' href='{{ asset("css/jplayer.blue.monday.css") }}' type='text/css'
        media='all' />
    <link rel='stylesheet' id='mfn-responsive-css' href='{{ asset("css/responsive.css") }}' type='text/css'
        media='all' />
    <link rel='stylesheet' id='elementor-icons-css' href='{{ asset("css/elementor-icons.min.css") }}' type='text/css'
        media='all' />
    <link rel='stylesheet' id='elementor-frontend-css' href='{{ asset("css/frontend.min.css") }}' type='text/css'
        media='all' />
    <link rel='stylesheet' id='elementor-post-88-css' href='{{ asset("css/post-88.css") }}' type='text/css'
        media='all' />
    <link rel='stylesheet' id='font-awesome-5-all-css' href='{{ asset("css/all.min.css") }}' type='text/css'
        media='all' />
    <link rel='stylesheet' id='font-awesome-4-shim-css' href=' {{ asset("css/v4-shims.min.css") }}' type='text/css'
        media='all' />
    <link rel='stylesheet' id='elementor-post-8-css' href='{{ asset("css/post-8.css") }}' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-post-inline' href='{{ asset("css/inline.css") }}' type='text/css'
        media='all' />
    <link rel='stylesheet' id='mfn-elementor-css' href='{{ asset("css/elementor.css") }}' type='text/css' media='all' />
    <link rel='stylesheet' id='rs-plugin-settings-css' href='{{ asset("css/rs6.css") }}' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-icons-shared-0-css' href='{{ asset("css/fontawesome.min.css") }}'
        type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-icons-fa-solid-css' href='{{ asset("css/solid.min.css") }}' type='text/css'
        media='all' />
    <link rel='stylesheet' href='{{ asset("css/custom.css") }}' type='text/css' media='all' />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <script type='text/javascript' src='{{ asset("js/jquery.min.js") }}' id='jquery-core-js'></script>
    <script type='text/javascript' src='{{ asset("js/jquery-migrate.min.js") }}' id='jquery-migrate-js'></script>
    <script type='text/javascript' src='{{ asset("js/v4-shims.min.js") }}' id='font-awesome-4-shim-js'></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>



    <!-- /end HFCM by 99 Robots -->
    <meta name="generator"
        content="Powered by Slider Revolution 6.5.5 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
    <script type="text/javascript">
        function setREVStartSize(e){
            //window.requestAnimationFrame(function() {				 
                window.RSIW = window.RSIW===undefined ? window.innerWidth : window.RSIW;	
                window.RSIH = window.RSIH===undefined ? window.innerHeight : window.RSIH;	
                try {								
                    var pw = document.getElementById(e.c).parentNode.offsetWidth,
                    newh;
                    pw = pw===0 || isNaN(pw) ? window.RSIW : pw;
                    e.tabw = e.tabw===undefined ? 0 : parseInt(e.tabw);
                    e.thumbw = e.thumbw===undefined ? 0 : parseInt(e.thumbw);
                    e.tabh = e.tabh===undefined ? 0 : parseInt(e.tabh);
                    e.thumbh = e.thumbh===undefined ? 0 : parseInt(e.thumbh);
                    e.tabhide = e.tabhide===undefined ? 0 : parseInt(e.tabhide);
                    e.thumbhide = e.thumbhide===undefined ? 0 : parseInt(e.thumbhide);
                    e.mh = e.mh===undefined || e.mh=="" || e.mh==="auto" ? 0 : parseInt(e.mh,0);		
                    if(e.layout==="fullscreen" || e.l==="fullscreen") 						
                        newh = Math.max(e.mh,window.RSIH);					
                    else{					
                        e.gw = Array.isArray(e.gw) ? e.gw : [e.gw];
                        for (var i in e.rl) if (e.gw[i]===undefined || e.gw[i]===0) e.gw[i] = e.gw[i-1];					
                        e.gh = e.el===undefined || e.el==="" || (Array.isArray(e.el) && e.el.length==0)? e.gh : e.el;
                        e.gh = Array.isArray(e.gh) ? e.gh : [e.gh];
                        for (var i in e.rl) if (e.gh[i]===undefined || e.gh[i]===0) e.gh[i] = e.gh[i-1];
                        var nl = new Array(e.rl.length),
                        ix = 0,						
                        sl;					
                        e.tabw = e.tabhide>=pw ? 0 : e.tabw;
                        e.thumbw = e.thumbhide>=pw ? 0 : e.thumbw;
                        e.tabh = e.tabhide>=pw ? 0 : e.tabh;
                        e.thumbh = e.thumbhide>=pw ? 0 : e.thumbh;					
                        for (var i in e.rl) nl[i] = e.rl[i]<window.RSIW ? 0 : e.rl[i];
                        sl = nl[0];									
                        for (var i in nl) if (sl>nl[i] && nl[i]>0) { sl = nl[i]; ix=i;}															
                        var m = pw>(e.gw[ix]+e.tabw+e.thumbw) ? 1 : (pw-(e.tabw+e.thumbw)) / (e.gw[ix]);					
                        newh =  (e.gh[ix] * m) + (e.tabh + e.thumbh);
                    }
                    var el = document.getElementById(e.c);
                    if (el!==null && el) el.style.height = newh+"px";					
                    el = document.getElementById(e.c+"_wrapper");
                    if (el!==null && el) {
                        el.style.height = newh+"px";
                        el.style.display = "block";
                    }
                } catch(e){
                    console.log("Failure at Presize of Slider:" + e)
                }					   
            //});
            };
    </script>

</head>

<body
    class="home page-template-default page page-id-8 everest-forms-no-js template-slider  color-custom style-simple button-custom layout-full-width no-content-padding no-shadows is-elementor header-classic header-fw sticky-header sticky-tb-color ab-hide subheader-both-center menu-line-below menuo-right menuo-no-borders footer-copy-center mobile-tb-center mobile-side-slide mobile-mini-mr-ll tablet-sticky mobile-header-mini mobile-sticky be-reg-2402 elementor-default elementor-kit-88 elementor-page elementor-page-8">
    <div id="Wrapper">
        <div id="Header_wrapper" class="">
            <header id="Header">
                <div class="header_placeholder_1">
                    Selling Price : <b><span id="myr">{{ $result }}</span> MYR</b> <a href="" class="buy_now"> Buy now</a>
                </div>
                <div id="Top_bar" class="loading">
                    <div class="container">
                        <div class="column one">
                            <div class="top_bar_left clearfix">
                                <div class="logo text-logo">
                                    <a class="logo_img" href="/" title="SEGI Solutions" data-height="60"
                                        data-padding="15"><img
                                            src="https://segiloan.s3.ap-southeast-1.amazonaws.com/global/segi_logo.png"></a>
                                    <a id="logo" href="/" title="SEGI Solutions" data-height="60"
                                        data-padding="15">SEGI</a>
                                </div>
                                <div class="menu_wrapper">
                                    <nav id="menu">
                                        <ul id="menu-main-menu" class="menu menu-main">
                                            <li id="menu-item-72"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-8 current_page_item">
                                                <a href="/"><span>Home</span></a>
                                            </li>
                                            <li id="menu-item-72"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home page_item">
                                                <a href="https://segi.bz/#/home"
                                                    target="_blank"><span>Trading</span></a>
                                            </li>
                                            <li id="menu-item-72"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home page_item">
                                                <a href=""><span>Bill Payment</span></a>
                                            </li>
                                        </ul>
                                        <ul id="menu-main-menu" class="menu menu-main menu-second">
                                            <li id="menu-item-72"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home page_item">
                                                <a href="/admin/auth/login"><span>Login</span></a>
                                            </li>
                                        </ul>
                                    </nav>
                                    <a class="responsive-menu-toggle " href="#"><i class="fa fa-bars"
                                            aria-hidden="true"></i></a>
                                </div>
                                <div class="secondary_menu_wrapper"></div>
                                <div class="banner_wrapper"></div>
                                <!-- <div class="search_wrapper">
                                        <form method="get" id="searchform" action="h/">
                                            <i class="icon_search icon-search-fine"></i>
                                            <a href="#" class="icon_close"><i class="icon-cancel-fine"></i></a>
                                            <input type="text" class="field" name="s" placeholder="Enter your search" />
                                            <input type="submit" class="display-none" value="" />
                                        </form>
                                    </div> -->
                            </div>
                            <!-- <div class="top_bar_right">
                                    <div class="top_bar_right_wrapper">
                                        <a id="search_button" href="#"><i class="icon-search-fine"></i></a>
                                    </div>
                                </div> -->
                        </div>
                    </div>
                </div>
                <div class="mfn-main-slider mfn-rev-slider">
                    <!-- START Home Corporation3_el REVOLUTION SLIDER 6.5.5 -->
                    <p class="rs-p-wp-fix"></p>
                    <rs-module-wrap id="rev_slider_2_1_wrapper" data-source="gallery"
                        style="visibility:hidden;background:transparent;padding:0;margin:0px auto;margin-top:0;margin-bottom:0;">
                        <rs-module id="rev_slider_2_1" style="" data-version="6.5.5">
                            <rs-slides>

                                <rs-slide style="position: absolute;" data-key="rs-2" data-title="Slide" data-in="o:0;"
                                    data-out="a:false;">
                                    <img src="{{asset('/images/sevenstorm.jpg')}}" title="corporation3-slider-bg"
                                        width="1920" height="775" class="rev-slidebg tp-rs-img rs-lazyload"
                                        data-lazyload="{{asset('/images/sevenstorm.jpg')}}"
                                        data-panzoom="d:10000;ss:110%;se:100;os:50px/50px;oe:0px/0;" data-no-retina>
                                    <rs-layer id="slider-2-slide-2-layer-0" data-type="text" data-rsp_ch="on"
                                        data-xy="x:l,l,c,c;xo:84px,84px,0,84px;y:m;yo:-51px,-51px,-63px,-63px;"
                                        data-text="w:normal;s:85,85,80,80;l:100,100,85,85;fw:500;a:left,left,center,center;"
                                        data-dim="w:1043,1043,600px,600px;" data-frame_0="x:34px,34px,21px,21px;"
                                        data-frame_1="st:100;sp:1000;sR:100;" data-frame_999="o:0;st:w;sR:7900;"
                                        style="z-index:5;font-family:'DM Sans';">
                                        为您量身订造的 <br />
                                        数码货币贷款方案
                                    </rs-layer>
                                    <a id="slider-2-slide-2-layer-1" class="rs-layer rev-btn" href="#contact-us"
                                        target="_self" data-type="button" data-color="#251117" data-rsp_ch="on"
                                        data-xy="x:l,l,c,c;xo:93px,93px,0,93px;y:m;yo:102px,102px,97px,97px;"
                                        data-text="w:normal;s:17;l:60;fw:700;" data-dim="minh:0px,0px,none,0px;"
                                        data-padding="r:40;l:40;" data-border="bor:4px,4px,4px,4px;"
                                        data-frame_0="x:50,50,31,31;" data-frame_1="st:150;sp:1000;sR:150;"
                                        data-frame_999="o:0;st:w;sR:7850;"
                                        data-frame_hover="c:#fff;bgc:#32b87b;bor:4px,4px,4px,4px;sp:200;e:power1.inOut;"
                                        style="z-index:6;background-color:#ffffff;font-family:'DM Sans';">
                                        联系我们
                                    </a>
                                    <rs-layer id="slider-2-slide-2-layer-2" data-type="text" data-rsp_ch="on"
                                        data-xy="xo:91px,91px,56px,56px;yo:539px,539px,338px,338px;"
                                        data-text="w:normal;s:21,21,12,12;l:27,27,16,16;" data-dim="h:52,52,31,31;"
                                        data-frame_999="o:0;st:w;" style="z-index:7;font-family:'Roboto';">
                                        免加盟金. 共创双赢. 创业圆梦<br />
                                    </rs-layer>
                                </rs-slide>
                            </rs-slides>
                        </rs-module>
                        <script type="text/javascript">
                            setREVStartSize({c: 'rev_slider_2_1',rl:[1240,1240,778,778],el:[775,775,960,960],gw:[1240,1240,778,778],gh:[775,775,960,960],type:'hero',justify:'',layout:'fullwidth',mh:"0"});if (window.RS_MODULES!==undefined && window.RS_MODULES.modules!==undefined && window.RS_MODULES.modules["revslider21"]!==undefined) {window.RS_MODULES.modules["revslider21"].once = false;window.revapi2 = undefined;window.RS_MODULES.checkMinimal()}
                        </script>
                    </rs-module-wrap>
                    <!-- END REVOLUTION SLIDER -->
                </div>
            </header>
        </div>

        <!-- mfn_hook_content_before -->
        <!-- mfn_hook_content_before -->
        <div id="Content">
            <div class="content_wrapper clearfix">
                <div class="sections_group">
                    <div class="entry-content" itemprop="mainContentOfPage">
                        <div class="mfn-builder-content"></div>
                        <div class="section the_content has_content">
                            <div class="section_wrapper">
                                <div class="the_content_wrapper is-elementor">
                                    <div data-elementor-type="wp-page" data-elementor-id="8"
                                        class="elementor elementor-8" data-elementor-settings="[]">
                                        <div class="elementor-section-wrap">
                                            <section
                                                class="elementor-section elementor-top-section elementor-element elementor-element-c54e391 elementor-section-content-middle elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                                data-id="c54e391" data-element_type="section"
                                                data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}">
                                                <div class="elementor-container elementor-column-gap-wider">
                                                    <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-75d6413"
                                                        data-id="75d6413" data-element_type="column">
                                                        <div class="elementor-widget-wrap elementor-element-populated">
                                                            <div class="elementor-element elementor-element-56d21fb elementor-widget elementor-widget-text-editor"
                                                                data-id="56d21fb" data-element_type="widget"
                                                                data-widget_type="text-editor.default">
                                                                <div class="elementor-widget-container">
                                                                    <h2>我们的使命解决<br /><span
                                                                            class="themecolor">您目前的燃眉之急</span>
                                                                        <br />加盟商助力计划
                                                                    </h2>
                                                                </div>
                                                            </div>
                                                            <div class="elementor-element elementor-element-a703681 elementor-widget elementor-widget-text-editor"
                                                                data-id="a703681" data-element_type="widget"
                                                                data-widget_type="text-editor.default">
                                                                <div class="elementor-widget-container">
                                                                    <h5>快速简便的申请方式，在收集完整的资料后，我们的团队将进行审核并在 3
                                                                        天工作日内处理和批准(如果合格)。</h5>
                                                                </div>
                                                            </div>
                                                            <div class="elementor-element elementor-element-bf0d5d9 elementor-widget elementor-widget-text-editor"
                                                                data-id="bf0d5d9" data-element_type="widget"
                                                                data-widget_type="text-editor.default">
                                                                <div class="elementor-widget-container">
                                                                    <h6><a href="#contact-us">联系我们<i
                                                                                class="fas fa-chevron-right ml-10"></i></a>
                                                                    </h6>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-ff7ba29"
                                                        data-id="ff7ba29" data-element_type="column">
                                                        <div class="elementor-widget-wrap elementor-element-populated">
                                                            <div class="elementor-element elementor-element-cf53bee elementor-widget elementor-widget-text-editor"
                                                                data-id="cf53bee" data-element_type="widget"
                                                                data-widget_type="text-editor.default">
                                                                <div class="elementor-widget-container">
                                                                    <h3>微型数码货币贷款</h3>
                                                                    <h3>商业数码货币贷款</h3>
                                                                    <!-- <h3>抵押贷款</h3> -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <section
                                                class="elementor-section elementor-top-section elementor-element elementor-element-6199bec elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                                data-id="6199bec" data-element_type="section"
                                                data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}">
                                                <div class="elementor-container elementor-column-gap-wider">
                                                    <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-1c992d4"
                                                        data-id="1c992d4" data-element_type="column">
                                                        <div class="elementor-widget-wrap elementor-element-populated">
                                                            <div class="elementor-element elementor-element-d6c71a6 elementor-widget elementor-widget-text-editor"
                                                                data-id="d6c71a6" data-element_type="widget"
                                                                data-widget_type="text-editor.default">
                                                                <div class="elementor-widget-container">
                                                                    <h2><span style="color: #ffffff;">加盟商助力计划</span>
                                                                    </h2>
                                                                </div>
                                                            </div>
                                                            <section
                                                                class="elementor-section elementor-inner-section elementor-element elementor-element-d5a0c52 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                                                data-id="d5a0c52" data-element_type="section">
                                                                <div
                                                                    class="elementor-container elementor-column-gap-narrow">
                                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-ad59d9d"
                                                                        data-id="ad59d9d" data-element_type="column">
                                                                        <div
                                                                            class="elementor-widget-wrap elementor-element-populated">
                                                                            <!-- <div class="elementor-element elementor-element-a580c7d elementor-widget elementor-widget-image" data-id="a580c7d" data-element_type="widget" data-widget_type="image.default">
                                                                                    <div class="elementor-widget-container">
                                                                                        <img width="298" height="51" src="{{ asset('images/corporation3-home-icon1.png') }}" class="attachment-large size-large" alt="" loading="lazy" srcset="{{ asset('images/corporation3-home-icon1.png') }} 298w, {{ asset('images/corporation3-home-icon1-260x44.png') }} 260w, {{ asset('images/corporation3-home-icon1-50x9.png') }} 50w, {{ asset('images/corporation3-home-icon1-150x26.png') }} 150w" sizes="(max-width: 298px) 100vw, 298px" />
                                                                                    </div>
                                                                                </div> -->
                                                                            <div class="elementor-element elementor-element-49c202d elementor-widget elementor-widget-text-editor"
                                                                                data-id="49c202d"
                                                                                data-element_type="widget"
                                                                                data-widget_type="text-editor.default">
                                                                                <div class="elementor-widget-container">
                                                                                    <h3><span
                                                                                            style="color: #ffffff;">微型数码货币贷款</span>
                                                                                    </h3>
                                                                                    <!-- <div style="text-align: center;">
                                                                                            <span style="color: #ffffff;">Annual Percentage Rate (APR) </span><br />
                                                                                            <span style="color: #ffffff;">利息为 4%-9% /年</span>
                                                                                        </div> -->
                                                                                    <div></div>
                                                                                    <ul class="custom_ul">
                                                                                        <li><span
                                                                                                style="color: #ffffff;">0
                                                                                                费用 </span></li>
                                                                                        <li><span
                                                                                                style="color: #ffffff;">贷款期:
                                                                                                1年 (依据借贷数额) </span></li>
                                                                                        <li><span
                                                                                                style="color: #ffffff;">数额:
                                                                                                5,000 SEGI – <span
                                                                                                    style="color:red">100,000</span>
                                                                                                SEGI </span></li>
                                                                                        <li><span
                                                                                                style="color: #ffffff;">律师合同公司承担
                                                                                            </span></li>
                                                                                        <li><span
                                                                                                style="color: #ffffff;">没隐藏费用
                                                                                            </span></li>
                                                                                        <li><span
                                                                                                style="color: #ffffff;">任何信贷评级
                                                                                            </span></li>
                                                                                        <li><span
                                                                                                style="color: #ffffff;">无抵押亦可借贷</span>
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-1f20e9a"
                                                                        data-id="1f20e9a" data-element_type="column">
                                                                        <div
                                                                            class="elementor-widget-wrap elementor-element-populated">
                                                                            <!-- <div class="elementor-element elementor-element-919ce08 elementor-widget elementor-widget-image" data-id="919ce08" data-element_type="widget" data-widget_type="image.default">
                                                                                    <div class="elementor-widget-container">
                                                                                        <img width="298" height="51" src="{{ asset('images/corporation3-home-icon2.png') }}" class="attachment-large size-large" alt="" loading="lazy" srcset="{{ asset('images/corporation3-home-icon2.png') }} 298w, {{ asset('images/corporation3-home-icon2-260x44.png') }} 260w, {{ asset('images/corporation3-home-icon2-50x9.png') }} 50w, {{ asset('images/corporation3-home-icon2-150x26.png') }} 150w" sizes="(max-width: 298px) 100vw, 298px" />
                                                                                    </div>
                                                                                </div> -->

                                                                            <div class="elementor-element elementor-element-5b0317a elementor-widget elementor-widget-text-editor"
                                                                                data-id="5b0317a"
                                                                                data-element_type="widget"
                                                                                data-widget_type="text-editor.default">
                                                                                <div class="elementor-widget-container">
                                                                                    <h3><span
                                                                                            style="color: #ffffff;">商业数码货币贷款</span>
                                                                                    </h3>
                                                                                    <!-- <div style="text-align: center;">
                                                                                            <span style="color: #ffffff;">Annual Percentage Rate (APR) </span><br />
                                                                                            <span style="color: #ffffff;">利息为 4%-9% /年</span>
                                                                                        </div> -->
                                                                                    <div></div>
                                                                                    <ul class="custom_ul">
                                                                                        <li><span
                                                                                                style="color: #ffffff;">0
                                                                                                费用 </span></li>
                                                                                        <li><span
                                                                                                style="color: #ffffff;">贷款期:
                                                                                                1年 (依据借贷数额) </span></li>
                                                                                        <li><span
                                                                                                style="color: #ffffff;">数额:
                                                                                                10,000 SEGI – <span
                                                                                                    style="color:red;">1,000,000</span>
                                                                                                SEGI </span></li>
                                                                                        <li><span
                                                                                                style="color: #ffffff;">律师合同公司承担
                                                                                            </span></li>
                                                                                        <li><span
                                                                                                style="color: #ffffff;">没隐藏费用
                                                                                            </span></li>
                                                                                        <li><span
                                                                                                style="color: #ffffff;">任何信贷评级
                                                                                            </span></li>
                                                                                        <li><span
                                                                                                style="color: #ffffff;">无抵押亦可借贷</span>
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!-- <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-16bd769" data-id="16bd769" data-element_type="column">
                                                                            <div class="elementor-widget-wrap elementor-element-populated">
                                                                                <div class="elementor-element elementor-element-a425b96 elementor-widget elementor-widget-image" data-id="a425b96" data-element_type="widget" data-widget_type="image.default">
                                                                                    <div class="elementor-widget-container">
                                                                                        <img width="298" height="51" src="{{ asset('images/corporation3-home-icon3.png') }}" class="attachment-large size-large" alt="" loading="lazy" srcset="{{ asset('images/corporation3-home-icon3.png') }} 298w, {{ asset('images/corporation3-home-icon3-260x44.png') }} 260w, {{ asset('images/corporation3-home-icon3-50x9.png') }} 50w, {{ asset('images/corporation3-home-icon3-150x26.png') }} 150w" sizes="(max-width: 298px) 100vw, 298px" />															
                                                                                    </div>
                                                                                </div>
                                                                
                                                                                <div class="elementor-element elementor-element-06784f6 elementor-widget elementor-widget-text-editor" data-id="06784f6" data-element_type="widget" data-widget_type="text-editor.default">
                                                                                    <div class="elementor-widget-container">
                                                                                        <h3 style="text-align: center;"><span style="color: #ffffff;">抵押贷款</span></h3>
                                                                                        <div style="text-align: center;">
                                                                                            <span style="color: #ffffff;">Annual Percentage Rate (APR) </span><br />
                                                                                            <span style="color: #ffffff;">利息为 4%-9% /年</span>
                                                                                        </div>
                                                                                        <div></div>
                                                                                        <ul>
                                                                                            <li><span style="color: #ffffff;">0 费用 </span></li>
                                                                                            <li><span style="color: #ffffff;">贷款期: 1年 &#8211; 10年 </span></li>
                                                                                            <li><span style="color: #ffffff;">数额: 15,000 – 250,000 </span></li>
                                                                                            <li><span style="color: #ffffff;">律师合同公司承担 </span></li>
                                                                                            <li><span style="color: #ffffff;">没隐藏费用 </span></li>
                                                                                            <li><span style="color: #ffffff;">任何信贷评级 </span></li>
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div> -->
                                                                </div>
                                                            </section>
                                                            <div class="elementor-element elementor-element-eaa6151 elementor-widget elementor-widget-spacer"
                                                                data-id="eaa6151" data-element_type="widget"
                                                                data-widget_type="spacer.default">
                                                                <div class="elementor-widget-container">
                                                                    <div class="elementor-spacer">
                                                                        <div class="elementor-spacer-inner"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="elementor-element elementor-element-71a8d3b elementor-widget elementor-widget-text-editor"
                                                                data-id="71a8d3b" data-element_type="widget"
                                                                data-widget_type="text-editor.default">
                                                                <div class="elementor-widget-container">
                                                                    <h3 style="text-align: center;">
                                                                        <span style="color: #ffffff;">
                                                                            最低贷款期限为 12 个月<br />
                                                                            Min. Repayment Period: 12 mths<br />
                                                                            <span style="color:red;">**</span>贷款 <span
                                                                                style="color:red;">10,000SEGI</span>
                                                                            以上需另审核批准<br/>须符合条规
                                                                        </span>
                                                                    </h3>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <!-- <section class="elementor-section elementor-top-section elementor-element elementor-element-d132ef2 elementor-section-content-middle elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="d132ef2" data-element_type="section" data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;}">
                                                    <div class="elementor-container elementor-column-gap-wider">
                                                        <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-bd4a74b" data-id="bd4a74b" data-element_type="column">
                                                            <div class="elementor-widget-wrap elementor-element-populated">
                                                                <div class="elementor-element elementor-element-c60fc3b elementor-widget elementor-widget-image" data-id="c60fc3b" data-element_type="widget" data-widget_type="image.default">
                                                                    <div class="elementor-widget-container">
                                                                        <img width="60" height="53" src="{{ asset('images/corporation3-home-icon7.png') }}" class="attachment-large size-large" alt="" loading="lazy" srcset="{{ asset('images/corporation3-home-icon7.png') }} 60w, {{ asset('images/corporation3-home-icon7-50x44.png') }} 50w" sizes="(max-width: 60px) 100vw, 60px" />
                                                                    </div>
                                                                </div>
                                                                
                                                                <div class="elementor-element elementor-element-8c1df60 elementor-widget elementor-widget-text-editor" data-id="8c1df60" data-element_type="widget" data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <h2>Loan Repayment Table</h2>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-c10eee6 elementor-widget elementor-widget-text-editor" data-id="c10eee6" data-element_type="widget" data-widget_type="text-editor.default">
                                                                    <div class="elementor-widget-container">
                                                                        <p>(Subject to adjustment upon agreement)</p>
                                                                    </div>
                                                                </div>
                                                                <div class="elementor-element elementor-element-3965de2 elementor-mobile-align-center elementor-widget elementor-widget-button" data-id="3965de2" data-element_type="widget" data-widget_type="button.default">
                                                                    <div class="elementor-widget-container">
                                                                        <div class="elementor-button-wrapper">
                                                                            <a href="/#contact-us" class="elementor-button-link elementor-button elementor-size-sm" role="button">
                                                                                <span class="elementor-button-content-wrapper">
                                                                                    <span class="elementor-button-text">联系我们</span>
                                                                                </span>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-e1e3ccc" data-id="e1e3ccc" data-element_type="column">
                                                            <div class="elementor-widget-wrap elementor-element-populated">
                                                                <div class="elementor-element elementor-element-45a5a37 elementor-widget elementor-widget-image" data-id="45a5a37" data-element_type="widget" data-widget_type="image.default">
                                                                    <div class="elementor-widget-container">
                                                                        <img width="672" height="187" src="{{ asset('images/image_2021-08-05_14-42-08.png') }}" class="attachment-large size-large" alt="" loading="lazy" srcset="{{ asset('images/image_2021-08-05_14-42-08.png') }} 672w, {{ asset('images/image_2021-08-05_14-42-08-300x83.png') }} 300w, {{ asset('images/image_2021-08-05_14-42-08-260x72.png') }} 260w, {{ asset('images/image_2021-08-05_14-42-08-50x14.png') }} 50w, {{ asset('images/image_2021-08-05_14-42-08-150x42.png') }} 150w" sizes="(max-width: 672px) 100vw, 672px" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section> -->
                                            <section
                                                class="elementor-section elementor-top-section elementor-element elementor-element-d4eb12f elementor-section-stretched elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                                data-id="d4eb12f" data-element_type="section"
                                                data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;}">
                                                <div class="elementor-container elementor-column-gap-wider">
                                                    <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-d3c1b26"
                                                        data-id="d3c1b26" data-element_type="column">
                                                        <div class="elementor-widget-wrap elementor-element-populated">
                                                            <div class="elementor-element elementor-element-168b539 elementor-widget elementor-widget-text-editor"
                                                                data-id="168b539" data-element_type="widget"
                                                                data-widget_type="text-editor.default">
                                                                <div class="elementor-widget-container">
                                                                    <h4>找寻一个最适合您的数码货币贷款方案</h4>
                                                                    <h3><span class="themecolor">为什么选择我们？</span></h3>
                                                                    <p>贷款偿还期的灵活性很重要，我们将根据您的需求而设计。我们的顾问随时准备协助您克服财务挑战。</p>
                                                                </div>
                                                            </div>
                                                            <div class="elementor-element elementor-element-31885a7 elementor-widget elementor-widget-button"
                                                                data-id="31885a7" data-element_type="widget"
                                                                data-widget_type="button.default">
                                                                <div class="elementor-widget-container">
                                                                    <div class="elementor-button-wrapper">
                                                                        <a href="/#contact-us"
                                                                            class="elementor-button-link elementor-button elementor-size-sm"
                                                                            role="button">
                                                                            <span
                                                                                class="elementor-button-content-wrapper">
                                                                                <span
                                                                                    class="elementor-button-text">联系我们</span>
                                                                            </span>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="elementor-element elementor-element-054076c elementor-widget elementor-widget-text-editor"
                                                                data-id="054076c" data-element_type="widget"
                                                                data-widget_type="text-editor.default">
                                                                <div class="elementor-widget-container">
                                                                    <h6>我们的加盟商助力计划提供商业或个人数码货币贷款，低至 0%的利息。</h6>
                                                                    <h4>低利息</h4>
                                                                </div>
                                                            </div>
                                                            <div class="elementor-element elementor-element-244f93b elementor-widget elementor-widget-text-editor"
                                                                data-id="244f93b" data-element_type="widget"
                                                                data-widget_type="text-editor.default">
                                                                <div class="elementor-widget-container">
                                                                    <h6>我们有上千的满意客户。他们也向朋友推荐我们的服务。</h6>
                                                                    <h4>保证满意</h4>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-78dd2e0"
                                                        data-id="78dd2e0" data-element_type="column">
                                                        <div class="elementor-widget-wrap elementor-element-populated">
                                                            <div class="elementor-element elementor-element-50dbcdb elementor-widget elementor-widget-text-editor"
                                                                data-id="50dbcdb" data-element_type="widget"
                                                                data-widget_type="text-editor.default">
                                                                <div class="elementor-widget-container">
                                                                    <h6>不管你是被什么机构列入黑名单，我们都愿意为您服务。</h6>
                                                                    <h4>被列入黑名单者也可加入助力计划</h4>
                                                                </div>
                                                            </div>
                                                            <div class="elementor-element elementor-element-f9c3831 elementor-widget elementor-widget-text-editor"
                                                                data-id="f9c3831" data-element_type="widget"
                                                                data-widget_type="text-editor.default">
                                                                <div class="elementor-widget-container">
                                                                    <h6>当我们的到完整资料后，如批准​ ，我们会在 3 天内批准贷款申请。</h6>
                                                                    <h4>快速简单程序</h4>
                                                                </div>
                                                            </div>
                                                            <div class="elementor-element elementor-element-5dfcdfb elementor-widget elementor-widget-text-editor"
                                                                data-id="5dfcdfb" data-element_type="widget"
                                                                data-widget_type="text-editor.default">
                                                                <div class="elementor-widget-container">
                                                                    <h6>我们确保客户的资料隐私和保密处理。</h6>
                                                                    <h4>绝对保密</h4>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <section
                                                class="elementor-section elementor-top-section elementor-element elementor-element-2b1ae36 elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                                data-id="2b1ae36" data-element_type="section"
                                                data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}">
                                                <div class="elementor-container elementor-column-gap-wider">
                                                    <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-19601ac"
                                                        data-id="19601ac" data-element_type="column">
                                                        <div class="elementor-widget-wrap elementor-element-populated">
                                                            <section
                                                                class="elementor-section elementor-inner-section elementor-element elementor-element-aae5c13 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                                                data-id="aae5c13" data-element_type="section">
                                                                <div
                                                                    class="elementor-container elementor-column-gap-default">
                                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-f03c8d4"
                                                                        data-id="f03c8d4" data-element_type="column">
                                                                        <div
                                                                            class="elementor-widget-wrap elementor-element-populated">
                                                                            <div class="elementor-element elementor-element-6d3ba21 elementor-widget elementor-widget-image"
                                                                                data-id="6d3ba21"
                                                                                data-element_type="widget"
                                                                                data-widget_type="image.default">
                                                                                <div class="elementor-widget-container">
                                                                                    <img width="780" height="552"
                                                                                        src="{{ asset('images/corporation3-home-pic1.jpeg') }}"
                                                                                        class="attachment-large size-large"
                                                                                        alt="" loading="lazy"
                                                                                        srcset="{{ asset('images/corporation3-home-pic1.jpeg') }} 780w, {{ asset('images/corporation3-home-pic1-300x212.jpeg') }} 300w, {{ asset('images/corporation3-home-pic1-768x544.jpeg') }} 768w, {{ asset('images/corporation3-home-pic1-206x146.jpeg') }} 206w, {{ asset('images/corporation3-home-pic1-50x35.jpeg') }} 50w, {{ asset('images/corporation3-home-pic1-106x75.jpeg') }} 106w"
                                                                                        sizes="(max-width: 780px) 100vw, 780px" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-fc85669"
                                                                        data-id="fc85669" data-element_type="column">
                                                                        <div
                                                                            class="elementor-widget-wrap elementor-element-populated">
                                                                            <div class="elementor-element elementor-element-4bddc25 elementor-widget elementor-widget-text-editor"
                                                                                data-id="4bddc25"
                                                                                data-element_type="widget"
                                                                                data-widget_type="text-editor.default">
                                                                                <div class="elementor-widget-container">
                                                                                    <h2>加盟商助力计划</h2>
                                                                                </div>
                                                                            </div>
                                                                            <div class="elementor-element elementor-element-a215dab elementor-align-left elementor-icon-list--layout-traditional elementor-list-item-link-full_width elementor-widget elementor-widget-icon-list"
                                                                                data-id="a215dab"
                                                                                data-element_type="widget"
                                                                                data-widget_type="icon-list.default">
                                                                                <div class="elementor-widget-container">
                                                                                    <ul
                                                                                        class="elementor-icon-list-items">
                                                                                        <li
                                                                                            class="elementor-icon-list-item">
                                                                                            <span
                                                                                                class="elementor-icon-list-icon"><i
                                                                                                    aria-hidden="true"
                                                                                                    class="fas fa-check"></i></span>
                                                                                            <span
                                                                                                class="elementor-icon-list-text">申请条件：具有实际行动能力之马来西亚公民，且为加盟商负责人或代表人。</span>
                                                                                        </li>
                                                                                        <li
                                                                                            class="elementor-icon-list-item">
                                                                                            <span
                                                                                                class="elementor-icon-list-icon"><i
                                                                                                    aria-hidden="true"
                                                                                                    class="fas fa-check"></i></span>
                                                                                            <span
                                                                                                class="elementor-icon-list-text">申请额度：最低为1万Segi
                                                                                                Coin 至无上限。</span>
                                                                                        </li>
                                                                                        <li
                                                                                            class="elementor-icon-list-item">
                                                                                            <span
                                                                                                class="elementor-icon-list-icon"><i
                                                                                                    aria-hidden="true"
                                                                                                    class="fas fa-check"></i></span>
                                                                                            <span
                                                                                                class="elementor-icon-list-text">借贷期限：1年</span>
                                                                                        </li>
                                                                                        <li
                                                                                            class="elementor-icon-list-item">
                                                                                            <span
                                                                                                class="elementor-icon-list-icon"><i
                                                                                                    aria-hidden="true"
                                                                                                    class="fas fa-check"></i></span>
                                                                                            <span
                                                                                                class="elementor-icon-list-text">借贷利率：
                                                                                                0%</span>
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </section>
                                                            <div class="join_container">
                                                                <img
                                                                    src="https://segiloan.s3.ap-southeast-1.amazonaws.com/global/join2.png">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <section
                                                class="elementor-section elementor-top-section elementor-element elementor-element-2b1ae361 elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                                data-id="2b1ae361" data-element_type="section"
                                                data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}">
                                                <div class="multi-platform">
                                                    <div class="d-flext container">
                                                        <div class="left text-center d-flex align-items-center">
                                                            <div class="d-flex flex-column align-items-center" style="width:100%">
                                                                <h2>下载SEGI移动端</h2>
                                                                <p>随时随地，开启掌上交易</p>
                                                                <div class="qr-code-box">
                                                                    <img src="/images/barcode.jpeg" style="display:inline-block;width:200px">
                                                                    <div class="label"><img class="scan-icon" src="/images/scanner_icon.png">  立即扫码下载</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <section class="elementor-section elementor-top-section elementor-element elementor-element-video-container elementor-section-stretched elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                            data-id="video-container" data-element_type="section"
                                            data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;,&quot;background_background&quot;:&quot;classic&quot;}"">
                                                <div class="elementor-container elementor-column-gap-wider">
                                                    <iframe width="853" height="480" src="https://www.youtube.com/embed/7pXJJMq7tHE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                </div>
                                            </section>
                                            <section
                                                class="elementor-section elementor-top-section elementor-element elementor-element-64f3314 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                                data-id="64f3314" data-element_type="section" id="contact-us">
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-5e6dfa9"
                                                        data-id="5e6dfa9" data-element_type="column">
                                                        <div class="elementor-widget-wrap elementor-element-populated">
                                                            <div class="elementor-element elementor-element-f995cbc elementor-widget elementor-widget-spacer"
                                                                data-id="f995cbc" data-element_type="widget"
                                                                data-widget_type="spacer.default">
                                                                <div class="elementor-widget-container">
                                                                    <div class="elementor-spacer">
                                                                        <div class="elementor-spacer-inner"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="elementor-element elementor-element-f47b78b elementor-widget elementor-widget-heading"
                                                                data-id="f47b78b" data-element_type="widget"
                                                                data-widget_type="heading.default">
                                                                <div class="elementor-widget-container">
                                                                    <h2
                                                                        class="elementor-heading-title elementor-size-default">
                                                                        联系我们</h2>
                                                                </div>
                                                            </div>
                                                            <div class="elementor-element elementor-element-f1f0bd9 elementor-widget elementor-widget-text-editor"
                                                                data-id="f1f0bd9" data-element_type="widget"
                                                                data-widget_type="text-editor.default">
                                                                <div class="elementor-widget-container">
                                                                    <div class="container mt-5">
                                                                        <div class="row justify-content-center">
                                                                            <div class="col-md-5 col-sm-12 card">
                                                                                <img class="icon"
                                                                                    src="https://segiloan.s3.ap-southeast-1.amazonaws.com/global/feedback.png">
                                                                                <p class="con_title">General Enquiries /
                                                                                    Feedback</p>
                                                                                <p class="phone">011-56734990</p>
                                                                                <p>9.00am - 6.00pm (Mon - Sun)</p>
                                                                                <a
                                                                                    href="maito:segi@segi.space">segi@segi.space</a>
                                                                            </div>
                                                                            <div class="col-md-5 col-sm-12 card">
                                                                                <img class="icon"
                                                                                    src="https://segiloan.s3.ap-southeast-1.amazonaws.com/global/whatapps.png">
                                                                                <p class="con_title">Chat with us
                                                                                    directly</p>
                                                                                <div class="row">
                                                                                    <div class="col-4">
                                                                                        <img
                                                                                            src="https://segiloan.s3.ap-southeast-1.amazonaws.com/global/whatapp_barcode.png">
                                                                                    </div>
                                                                                    <div class="col-8 contact-div">
                                                                                        <p>WhatsApp ID</p>
                                                                                        <a
                                                                                            href="tel:011-56734990">+011-56734990</a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row justify-content-center">
                                                                            <div class="col-md-5 col-sm-12 card">
                                                                                <img class="icon"
                                                                                    src="https://segiloan.s3.ap-southeast-1.amazonaws.com/global/opportunity.png">
                                                                                <p class="con_title">Opportunity</p>
                                                                                <p class="mb-0">Collaborations /
                                                                                    Partnerships</p>
                                                                                <a href="maito:segi@segi.space"
                                                                                    class="mb-3">segi@segi.space</a>
                                                                                <p class="mb-0">Click to visit our page:
                                                                                    <a href="https://segi.bz/#/home"
                                                                                        target="_blank">https//segi.bz</a>
                                                                                </p>
                                                                                <!-- <p class="mb-0" >Collaborations / Partnerships</p>
                                                                                    <a href="">partnership@carsome.my</a> -->
                                                                            </div>
                                                                            <div class="col-md-5 col-sm-12 card">
                                                                                <img class="icon"
                                                                                    src="https://segiloan.s3.ap-southeast-1.amazonaws.com/global/location.png">
                                                                                <p class="con_title">Corporate Office
                                                                                    Address</p>
                                                                                <p><b>SEGI HQ</b></p>
                                                                                <p>B-0-3,Phoenix Business Park,Jln
                                                                                    2/142A Megan Phoenix 56000 cheras,
                                                                                    KL Malaysia</p>
                                                                                <!-- <a href="">011-56734990</a> -->
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!-- <p>
                                                                            <div class="everest-forms">
                                                                                <div class="evf-container default" id="evf-120">
                                                                                    <form id="evf-form-120" class="everest-form" data-formid="120" data-ajax_submission="0" method="post" enctype="multipart/form-data" action="/">
                                                                                        <div class="evf-field-container">
                                                                                            <input type="hidden" id="_wpnonce" name="_wpnonce" value="fef0958bc7" />
                                                                                            <input type="hidden" name="_wp_http_referer" value="/" />
                                                                                            <div class="evf-frontend-row" data-row="row_1">
                                                                                                <div class="evf-frontend-grid evf-grid-1" data-grid="grid_1">
                                                                                                    <div id="evf-120-field_ys0GeZISRs-1-container" class="evf-field evf-field-first-name form-row validate-required" data-required-field-message="This field is required." data-field-id="ys0GeZISRs-1">
                                                                                                        <label class="evf-field-label" for="evf-120-field_ys0GeZISRs-1">
                                                                                                            <span class="evf-label">姓名</span> 
                                                                                                            <abbr class="required" title="Required">*</abbr>
                                                                                                        </label>
                                                                                                        <input type="text" id="evf-120-field_ys0GeZISRs-1" class="input-text" name="everest_forms[form_fields][ys0GeZISRs-1]" required>
                                                                                                    </div>
                                                                                                    <div id="evf-120-field_r9JFAvs9Qm-5-container" class="evf-field evf-field-number form-row validate-required" data-required-field-message="Please enter a valid number." data-field-id="r9JFAvs9Qm-5">
                                                                                                        <label class="evf-field-label" for="evf-120-field_r9JFAvs9Qm-5">
                                                                                                            <span class="evf-label">WhatsApp 号码 </span> 
                                                                                                            <abbr class="required" title="Required">*</abbr>
                                                                                                        </label>
                                                                                                        <input type="number" id="evf-120-field_r9JFAvs9Qm-5" class="input-text" name="everest_forms[form_fields][r9JFAvs9Qm-5]" step="1" required />
                                                                                                    </div>
                                                                                                    <div id="evf-120-field_66FR384cge-3-container" class="evf-field evf-field-text form-row validate-required" data-required-field-message="This field is required." data-field-id="66FR384cge-3">
                                                                                                        <label class="evf-field-label" for="evf-120-field_66FR384cge-3">
                                                                                                            <span class="evf-label">提问</span> 
                                                                                                            <abbr class="required" title="Required">*</abbr>
                                                                                                        </label>
                                                                                                        <input type="text" id="evf-120-field_66FR384cge-3" class="input-text" name="everest_forms[form_fields][66FR384cge-3]" required>
                                                                                                    </div>
                                                                                                    <div id="evf-120-field_emW7mA8aJ7-6-container" class="evf-field evf-field-select form-row" data-field-id="emW7mA8aJ7-6">
                                                                                                        <label class="evf-field-label" for="evf-120-field_emW7mA8aJ7-6">
                                                                                                            <span class="evf-label">贷款数额</span> 
                                                                                                        </label>
                                                                                                        <select id="evf-120-field_emW7mA8aJ7-6" class="input-text" name="everest_forms[form_fields][emW7mA8aJ7-6]">
                                                                                                            <option value="RM5,000 - RM10,000" >RM5,000 &#8211; RM10,000</option>
                                                                                                            <option value="RM10,001 - RM20,000" >RM10,001 &#8211; RM20,000</option>
                                                                                                            <option value="RM20,001 - RM50,000" >RM20,001 &#8211; RM50,000</option>
                                                                                                            <option value="RM100,001 - RM200,000" >RM100,001 &#8211; RM200,000</option>
                                                                                                            <option value="RM200,001 - RM300,000" >RM200,001 &#8211; RM300,000</option>
                                                                                                            <option value="RM300,001 - RM400,000" >RM300,001 &#8211; RM400,000</option>
                                                                                                            <option value="RM400,001 - RM500,000" >RM400,001 &#8211; RM500,000</option>
                                                                                                        </select>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="evf-honeypot-container evf-field-hp">
                                                                                            <label for="evf-120-field-hp" class="evf-field-label">Name</label>
                                                                                            <input type="text" name="everest_forms[hp]" id="evf-120-field-hp" class="input-text">
                                                                                        </div>
                                                                                        <div class="evf-submit-container " >
                                                                                            <input type="hidden" name="everest_forms[id]" value="120">
                                                                                            <input type="hidden" name="everest_forms[author]" value="1">
                                                                                            <input type="hidden" name="everest_forms[post_id]" value="8">
                                                                                            <button type='submit' name='everest_forms[submit]' class='everest-forms-submit-button button evf-submit ' id='evf-submit-120' value='evf-submit' data-process-text="发送中。。。" conditional_rules='""' conditional_id='evf-submit-120' >确认</button>
                                                                                        </div>
                                                                                    </form>
                                                                                </div>
                                                                            </div>
                                                                        </p> -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="section section-page-footer">
                            <div class="section_wrapper clearfix">
                                <div class="column one page-pager"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- mfn_hook_content_after -->
        <!-- mfn_hook_content_after -->
        <footer id="Footer" class="clearfix ">
            <!-- <div class="footer_action">
                    <div class="container">
                        <div class="column one column_column">
                            <p style="text-align: center;">地址: No 38 , Jalan PJS 7/13 Bandar Sunway 46150 Petaling Jaya Selangor. <br />周一 到 周五: 9am – 9pm <br />周六 & 周日: 9am – 3pm</p>
                        </div>
                    </div>
                </div>
                <div class="widgets_wrapper center">
                    <div class="container">
                        <div class="column one-third">
                            <aside id="custom_html-5" class="widget_text widget widget_custom_html">
                                <div class="textwidget custom-html-widget"></div>
                            </aside>
                        </div>
                        <div class="column two-third">
                            <aside id="custom_html-6" class="widget_text widget widget_custom_html">
                                <div class="textwidget custom-html-widget"></div>
                            </aside>
                        </div>
                    </div>
                </div> -->
            <div class="footer_copy">
                <div class="container">
                    <div class="column one">
                        <div class="copyright">©2020. SEGI All Rights Reserved</div>
                        <ul class="social"></ul>
                    </div>
                </div>
            </div>
        </footer>
    </div>

    <div id="Side_slide" class="right dark" data-width="250">
        <div class="close-wrapper">
            <a href="#" class="close"><i class="fa fa-times" aria-hidden="true"></i></a>
        </div>
        <!-- <div class="extras">
                <div class="extras-wrapper">
                    <a class="icon search" href="#"><i class="icon-search-fine"></i></a>
                </div>
            </div> -->
        <!-- <div class="search-wrapper">
                <form id="side-form" method="get" action="/">
                    <input type="text" class="field" name="s" placeholder="Enter your search" />
                    <input type="submit" class="display-none" value="" />
                    <a class="submit" href="#"><i class="icon-search-fine"></i></a>
                </form>
            </div> -->
        <div class="lang-wrapper"></div>
        <div class="menu_wrapper"></div>
        <ul class="social"></ul>
    </div>
    <div id="body_overlay"></div>

    <!-- mfn_hook_bottom -->
    <!-- mfn_hook_bottom -->
    <script type="text/javascript">
        window.RS_MODULES = window.RS_MODULES || {};
            window.RS_MODULES.modules = window.RS_MODULES.modules || {};
            window.RS_MODULES.waiting = window.RS_MODULES.waiting || [];
            window.RS_MODULES.defered = true;
            window.RS_MODULES.moduleWaiting = window.RS_MODULES.moduleWaiting || {};
            window.RS_MODULES.type = 'compiled';
    </script>

    <script type="text/javascript">
        var c = document.body.className;
            c = c.replace( /everest-forms-no-js/, 'everest-forms-js' );
            document.body.className = c;
    </script>
    <script type="text/javascript">
        if(typeof revslider_showDoubleJqueryError === "undefined") {function revslider_showDoubleJqueryError(sliderID) {console.log("You have some jquery.js library include that comes after the Slider Revolution files js inclusion.");console.log("To fix this, you can:");console.log("1. Set 'Module General Options' -> 'Advanced' -> 'jQuery & OutPut Filters' -> 'Put JS to Body' to on");console.log("2. Find the double jQuery.js inclusion and remove it");return "Double Included jQuery Library";}}
    </script>

    <script type='text/javascript' src='{{ asset("js/rbtools.min.js") }}' defer async id='tp-tools-js'></script>
    <script type='text/javascript' src='{{ asset("js/rs6.min.js") }}' defer async id='revmin-js'></script>
    <script type='text/javascript' src='{{ asset("js/core.min.js") }}' id='jquery-ui-core-js'></script>
    <script type='text/javascript' src='{{ asset("js/tabs.min.js") }}' id='jquery-ui-tabs-js'></script>
    <script type='text/javascript' id='mfn-plugins-js-extra'>
        /* <![CDATA[ */
            var mfn = {"mobileInit":"1240","parallax":"translate3d","responsive":"1","sidebarSticky":"","lightbox":{"disable":false,"disableMobile":false,"title":false},"slider":{"blog":0,"clients":0,"offer":0,"portfolio":0,"shop":0,"slider":0,"testimonials":0},"ajax":""};
            /* ]]> */
    </script>

    <script type='text/javascript' src='{{ asset("js/plugins.js") }}' id='mfn-plugins-js'></script>
    <script type='text/javascript' src='{{ asset("js/menu.js") }}' id='mfn-menu-js'></script>
    <script type='text/javascript' src='{{ asset("js/animations.min.js") }}' id='mfn-animations-js'></script>
    <script type='text/javascript' src='{{ asset("js/jplayer.min.js") }}' id='mfn-jplayer-js'></script>
    <script type='text/javascript' src='{{ asset("js/translate3d.js") }}' id='mfn-parallax-js'></script>
    <script type='text/javascript' src='{{ asset("js/scripts.js") }}' id='mfn-scripts-js'></script>
    <script type='text/javascript' src='{{ asset("js/wp-embed.min.js") }}' id='wp-embed-js'></script>
    <script type='text/javascript' src='{{ asset("js/jquery.inputmask.bundle.min.js") }}' id='inputmask-js'></script>
    <script type='text/javascript' src='{{ asset("js/jquery.validate.min.js") }}' id='jquery-validate-js'></script>
    <script type='text/javascript' src='{{ asset("js/everest-forms.min.js") }}' id='everest-forms-js'></script>
    <script type='text/javascript' src='{{ asset("js/ajax-submission.min.js") }}' id='everest-forms-ajax-submission-js'>
    </script>
    <script type='text/javascript' src='{{ asset("js/webpack.runtime.min.js") }}' id='elementor-webpack-runtime-js'>
    </script>
    <script type='text/javascript' src='{{ asset("js/frontend-modules.min.js") }}' id='elementor-frontend-modules-js'>
    </script>
    <script type='text/javascript' src='{{ asset("js/waypoints.min.js") }}' id='elementor-waypoints-js'></script>
    <script type='text/javascript' src='{{ asset("js/swiper.min.js") }}' id='swiper-js'></script>
    <script type='text/javascript' src='{{ asset("js/share-link.min.js") }}' id='share-link-js'></script>
    <script type='text/javascript' src='{{ asset("js/dialog.min.js") }}' id='elementor-dialog-js'></script>
    <script type='text/javascript' id='elementor-frontend-js-before'>
        var elementorFrontendConfig = {"environmentMode":{"edit":false,"wpPreview":false,"isScriptDebug":false},"i18n":{"shareOnFacebook":"Share on Facebook","shareOnTwitter":"Share on Twitter","pinIt":"Pin it","download":"Download","downloadImage":"Download image","fullscreen":"Fullscreen","zoom":"Zoom","share":"Share","playVideo":"Play Video","previous":"Previous","next":"Next","close":"Close"},"is_rtl":false,"breakpoints":{"xs":0,"sm":480,"md":768,"lg":960,"xl":1440,"xxl":1600},"responsive":{"breakpoints":{"mobile":{"label":"Mobile","value":767,"default_value":767,"direction":"max","is_enabled":true},"mobile_extra":{"label":"Mobile Extra","value":880,"default_value":880,"direction":"max","is_enabled":false},"tablet":{"label":"Tablet","value":1024,"default_value":1024,"direction":"max","is_enabled":true},"tablet_extra":{"label":"Tablet Extra","value":1200,"default_value":1200,"direction":"max","is_enabled":false},"laptop":{"label":"Laptop","value":1366,"default_value":1366,"direction":"max","is_enabled":false},"widescreen":{"label":"Widescreen","value":2400,"default_value":2400,"direction":"min","is_enabled":false}}},"version":"3.4.6","is_static":false,"experimentalFeatures":{"e_dom_optimization":true,"a11y_improvements":true,"e_import_export":true,"landing-pages":true,"elements-color-picker":true,"admin-top-bar":true},"urls":{"assets":""},"settings":{"page":[],"editorPreferences":[]},"kit":{"stretched_section_container":"#Wrapper","active_breakpoints":["viewport_mobile","viewport_tablet"],"global_image_lightbox":"yes","lightbox_enable_counter":"yes","lightbox_enable_fullscreen":"yes","lightbox_enable_zoom":"yes","lightbox_enable_share":"yes","lightbox_title_src":"title","lightbox_description_src":"description"},"post":{"id":8,"title":"SEGI%20Solutions%20%E2%80%93%20%E6%88%91%E4%BB%AC%E6%98%AF%E4%B8%80%E9%97%B4%E9%A9%AC%E6%9D%A5%E8%A5%BF%E4%BA%9A%E6%8B%A5%E6%9C%89%E8%89%AF%E5%A5%BD%E4%BF%A1%E8%AA%89%E7%9A%84%E8%B4%B7%E6%AC%BE%E5%85%AC%E5%8F%B8.%20%E6%9C%AC%E5%85%AC%E5%8F%B8%E5%B7%B2%E7%BB%8F%E6%8B%A5%E6%9C%898%E5%B9%B4%E7%9A%84%E8%90%A5%E4%B8%9A%E7%BB%8F%E9%AA%8C%E3%80%82","excerpt":"","featuredImage":false}};
    </script>
    <script type='text/javascript' src='{{ asset("js/frontend.min.js") }}' id='elementor-frontend-js'></script>
    <script type='text/javascript' src='{{ asset("js/preloaded-modules.min.js") }}' id='preloaded-modules-js'></script>
    <script type="text/javascript" id="rs-initialisation-scripts">
        var	tpj = jQuery;

            var	revapi2;

            if(window.RS_MODULES === undefined) window.RS_MODULES = {};
            if(RS_MODULES.modules === undefined) RS_MODULES.modules = {};
            RS_MODULES.modules["revslider21"] = {init:function() {
                window.revapi2 = window.revapi2===undefined || window.revapi2===null || window.revapi2.length===0  ? document.getElementById("rev_slider_2_1") : window.revapi2;
                if(window.revapi2 === null || window.revapi2 === undefined || window.revapi2.length==0) return;
                window.revapi2 = jQuery(window.revapi2);
                if(window.revapi2.revolution==undefined){ revslider_showDoubleJqueryError("rev_slider_2_1"); return;}
                revapi2.revolutionInit({
                        revapi:"revapi2",
                        sliderType:"hero",
                        DPR:"dpr",
                        sliderLayout:"fullwidth",
                        visibilityLevels:"1240,1240,778,778",
                        gridwidth:"1240,1240,778,778",
                        gridheight:"775,775,960,960",
                        lazyType:"smart",
                        spinner:"spinner9",
                        perspective:600,
                        perspectiveType:"global",
                        spinnerclr:"#55c593",
                        editorheight:"775,768,960,720",
                        responsiveLevels:"1240,1240,778,778",
                        progressBar:{disableProgressBar:true},
                        navigation: {
                            onHoverStop:false
                        },
                        viewPort: {
                            global:true,
                            globalDist:"-200px",
                            enable:false
                        },
                        fallbacks: {
                            allowHTML5AutoPlayOnAndroid:true
                        },
                });
            }} // End of RevInitScript

            if (window.RS_MODULES.checkMinimal!==undefined) { window.RS_MODULES.checkMinimal();};
    </script>
    <script type="text/javascript">
        (function($) {
            setInterval(function(){
                $.get("/conversion", function(data){
                    $('#myr').text(data.segi);
                });
            }, 10000);
        })(jQuery);
       
    </script>

</body>

</html>