@if ($errors->any())
    @foreach ($errors->all() as $error)
        <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">×</span></button>
            <strong>{{ $error }}</strong>
        </div>
    @endforeach
@endif

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">更改</h3>
        <div class="box-tools">
            <div class="btn-group pull-right" style="margin-right: 5px">
                <a href="/admin/application" class="btn btn-sm btn-default" title="List"><i
                        class="fa fa-list"></i><span class="hidden-xs">&nbsp;列表</span></a>
            </div>
        </div>
    </div>
    <form action="/admin/application/updatePost" method="post" class="form-horizontal model-form-61eeae3022506"
        enctype="multipart/form-data">
        <div class="box-body">
            <div class="fields-group">
                <div class="col-md-12">
                    <div class="form-group  ">
                        <label for="new" class="col-sm-2  control-label">商家</label>
                        <div class="col-sm-8">
                            <span class="icheck">
                                <label class="radio-inline">
                                    <div class="iradio_minimal-blue custom-iradio" style="position: relative;">
                                        <input type="radio" name="new" value="0"
                                            {{ $application->new == 0 ? ' checked' : '' }} class="minimal new"
                                            style="position: absolute; opacity: 0;">
                                    </div>
                                    &nbsp;新的&nbsp;&nbsp;
                                </label>
                            </span>
                            <span class="icheck">
                                <label class="radio-inline">
                                    <div class="iradio_minimal-blue" style="position: relative;">
                                        <input type="radio" name="new" value="1"
                                            {{ $application->new == 1 ? ' checked' : '' }} class="minimal new"
                                            style="position: absolute; opacity: 0;">
                                    </div>
                                    &nbsp;现有的&nbsp;&nbsp;
                                </label>
                            </span>
                        </div>
                    </div>
                    <div class="form-group  ">
                        <label for="online_commerce" class="col-sm-2  control-label">网店</label>
                        <div class="col-sm-8">
                            <span class="icheck">
                                <label class="radio-inline">
                                    <div class="iradio_minimal-blue" style="position: relative;">
                                        <input type="radio" name="online_commerce" value="0"
                                            {{ $application->online_commerce == 0 ? ' checked' : '' }}
                                            class="minimal online_commerce" style="position: absolute; opacity: 0;">
                                    </div>
                                    &nbsp;不是的&nbsp;&nbsp;
                                </label>
                            </span>
                            <span class="icheck">
                                <label class="radio-inline">
                                    <div class="iradio_minimal-blue" style="position: relative;">
                                        <input type="radio" name="online_commerce" value="1"
                                            {{ $application->online_commerce == 1 ? ' checked' : '' }}
                                            class="minimal online_commerce" style="position: absolute; opacity: 0;">
                                    </div>
                                    &nbsp;是的&nbsp;&nbsp;
                                </label>
                            </span>
                        </div>
                    </div>
                    <div class="form-group  ">
                        <label for="loanee_Id" class="col-sm-2 asterisk control-label">借贷者ID</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                                <input required="1" type="text" id="loanee_Id" name="loanee_Id"
                                    value="{{ $application->loanee_Id }}" class="form-control loanee_Id"
                                    placeholder="借贷者ID">
                            </div>
                        </div>
                    </div>
                    <div class="form-group  ">
                        <label for="loanee_name" class="col-sm-2 asterisk control-label">借贷者名称</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                                <input required="1" type="text" id="loanee_name" name="loanee_name"
                                    value="{{ $application->loanee_name }}" class="form-control loanee_name"
                                    placeholder="借贷者名称">
                            </div>
                        </div>
                    </div>
                    <div class="form-group  ">
                        <label for="loanee_company" class="col-sm-2 asterisk control-label">公司名称</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                                <input required="1" type="text" id="loanee_company" name="loanee_company"
                                    value="{{ $application->loanee_company }}" class="form-control loanee_company"
                                    placeholder="公司名称">
                            </div>
                        </div>
                    </div>
                    <div class="form-group  ">
                        <label for="loanee_company_address" class="col-sm-2 asterisk control-label">公司地址</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                                <input required="1" type="text" id="loanee_company_address"
                                    name="loanee_company_address" value="{{ $application->loanee_company_address }}"
                                    class="form-control loanee_company_address"
                                    placeholder="公司地址">
                            </div>
                        </div>
                    </div>
                    <div class="form-group  ">
                        <label for="loanee_company_registration_no" class="col-sm-2 asterisk control-label">公司注册号码</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                                <input required="1" type="text" id="loanee_company_registration_no"
                                    name="loanee_company_registration_no"
                                    value="{{ $application->loanee_company_registration_no }}"
                                    class="form-control loanee_company_registration_no"
                                    placeholder="公司注册号码">
                            </div>
                        </div>
                    </div>
                    <div class="form-group  ">
                        <label for="loanee_company_state" class="col-sm-2 asterisk control-label">公司地区</label>
                        <div class="col-sm-8">
                            <input type="hidden" required="1" name="loanee_company_state"
                                value="{{ $application->loanee_company_state }}">
                            <select class="form-control loanee_company_state select2-hidden-accessible" required="1"
                                style="width: 100%;" name="loanee_company_state" data-value="" tabindex="-1"
                                aria-hidden="true">
                                <option value=""></option>
                                <option value="Kedah"
                                    {{ $application->loanee_company_state == 'Kedah' ? ' selected' : '' }}>Kedah
                                </option>
                                <option value="Penang"
                                    {{ $application->loanee_company_state == 'Penang' ? ' selected' : '' }}>Penang
                                </option>
                                <option value="Johor"
                                    {{ $application->loanee_company_state == 'Johor' ? ' selected' : '' }}>Johor
                                </option>
                                <option value="Kelantan"
                                    {{ $application->loanee_company_state == 'Kelantan' ? ' selected' : '' }}>
                                    Kelantan</option>
                                <option value="Melaka"
                                    {{ $application->loanee_company_state == 'Melaka' ? ' selected' : '' }}>Melaka
                                </option>
                                <option value="Negeri Sembilan"
                                    {{ $application->loanee_company_state == 'Negeri Sembilan' ? ' selected' : '' }}>
                                    Negeri Sembilan</option>
                                <option value="Pahang"
                                    {{ $application->loanee_company_state == 'Pahang' ? ' selected' : '' }}>Pahang
                                </option>
                                <option value="Perak"
                                    {{ $application->loanee_company_state == 'Perak' ? ' selected' : '' }}>Perak
                                </option>
                                <option value="Perlis"
                                    {{ $application->loanee_company_state == 'Perlis' ? ' selected' : '' }}>Perlis
                                </option>
                                <option value="Selangor"
                                    {{ $application->loanee_company_state == 'Selangor' ? ' selected' : '' }}>
                                    Selangor</option>
                                <option value="Terengganu"
                                    {{ $application->loanee_company_state == 'Terengganu' ? ' selected' : '' }}>
                                    Terengganu</option>
                                <option value="Sabah"
                                    {{ $application->loanee_company_state == 'Sabah' ? ' selected' : '' }}>Sabah
                                </option>
                                <option value="Sarawak"
                                    {{ $application->loanee_company_state == 'Sarawak' ? ' selected' : '' }}>
                                    Sarawak</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group  ">
                        <label for="loanee_email" class="col-sm-2 asterisk control-label">公司邮件地址</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                                <input type="email" id="loanee_email" required="1" name="loanee_email"
                                    value="{{ $application->loanee_email }}" class="form-control loanee_email"
                                    placeholder="公司邮件地址">
                            </div>
                        </div>
                    </div>
                    <div class="form-group  ">
                        <label for="loanee_company_contact" class="col-sm-2 asterisk control-label">公司联络号码</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                                <input required="1" type="text" id="loanee_company_contact"
                                    name="loanee_company_contact" value="{{ $application->loanee_company_contact }}"
                                    class="form-control loanee_company_contact" placeholder="公司联络号码">
                            </div>
                        </div>
                    </div>
                    <div class="form-group  ">
                        <label for="category_id" class="col-sm-2 asterisk control-label">服务性质</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="category_id" value="{{ $application->category_id }}">
                            <select class="form-control category_id select2-hidden-accessible" style="width: 100%;"
                                name="category_id" data-value="" tabindex="-1" aria-hidden="true" required="1">
                                <option value=""></option>
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}"
                                        {{ $application->category_id == $category->id ? ' selected' : '' }}>
                                        {{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group  ">
                        <label for="loan_officer_name" class="col-sm-2 asterisk control-label">放款负责人</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                                <input required="1" type="text" id="loan_officer_name" name="loan_officer_name"
                                    value="{{ $application->loan_officer_name }}"
                                    class="form-control loan_officer_name" placeholder="放款负责人">
                            </div>
                        </div>
                    </div>
                    <div class="form-group  ">
                        <label for="loan_officer_contact" class="col-sm-2 asterisk control-label">放款负责联络</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                                <input required="1" type="text" id="loan_officer_contact" name="loan_officer_contact"
                                    value="{{ $application->loan_officer_contact }}"
                                    class="form-control loan_officer_contact" placeholder="放款负责联络">
                            </div>
                        </div>
                    </div>
                    <div class="form-group  ">
                        <label for="loan_officer_email" class="col-sm-2 asterisk control-label">放款负责邮件地址</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                                <input type="email" required="1" id="loan_officer_email" name="loan_officer_email"
                                    value="{{ $application->loan_officer_email }}"
                                    class="form-control loan_officer_email" placeholder="放款负责邮件地址">
                            </div>
                        </div>
                    </div>
                    <div class="form-group  ">
                        <label for="loan_amount" class="col-sm-2 asterisk control-label">借款数额</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                                <input type="number" step=".01" required="1" id="loan_amount" name="loan_amount"
                                    value="{{ $application->loan_amount }}" class="form-control loan_amount"
                                    placeholder="借款数额">
                            </div>
                        </div>
                    </div>
                    <div class="form-group  ">
                        <label for="loan_period" class="col-sm-2 asterisk control-label">借贷期限 （月）</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                                <input type="number" readonly required="1" id="loan_period" name="loan_period"
                                    value="{{ $application->loan_period }}" class="form-control loan_period"
                                    placeholder="借贷期限 （月）">
                            </div>
                        </div>
                    </div>
                    <div class="form-group  ">
                        <label for="loan_status" class="col-sm-2 asterisk control-label">借款状态</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="loan_status">
                            <select class="form-control loan_status select2-hidden-accessible" style="width: 100%;"
                                name="loan_status" data-value="" tabindex="-1" aria-hidden="true">
                                <option value=""></option>
                                <option value="0" {{ $application->loan_status == 0 ? ' selected' : '' }}>冻结</option>
                                <option value="1" {{ $application->loan_status == 1 ? ' selected' : '' }}>运行中</option>
                                <option value="2" {{ $application->loan_status == 2 ? ' selected' : '' }}>完成</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group  ">
                        <label for="file_url" class="col-sm-2 control-label">文件上载</label>
                        <div class="col-sm-8">
                            <div class="file-input">
                                <input class="file_url_a" name="file" type="file" class="file">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        
        <div id="has-many-installments" class="has-many-installments">
            <div class="form-group">
                <div class="col-sm-2">
                    <h1 class="sub-form ">偿还记录</h1>
                </div>
                <div class="col-sm-8 text-right">
                    <div class="add btn btn-success btn-sm"><i class="fa fa-save"></i>&nbsp;创建</div>
                </div>
            </div>
            <div class="has-many-installments-forms">
                @foreach($application->installments as $key => $installment)
                    <div class="has-many-installments-form fields-group">
                        <div class="form-group  ">
                            <label for="payable_date" class="col-sm-2 asterisk control-label">偿还日期</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span>
                                    <input required="1" style="width: 110px" type="text" id="payable_date"
                                        name="installments[{{$installment->id}}][payable_date]" value="{{ $installment->payable_date}}"
                                        class="form-control installments payable_date" placeholder="偿还日期" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group  ">
                            <label for="total_paid_months" class="col-sm-2 asterisk control-label">偿还期限（月）</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                                    <input required="1" type="number" id="total_paid_months" name="installments[{{$installment->id}}][total_paid_months]"
                                        value="{{ $installment->total_paid_months}}" class="form-control installments total_paid_months"
                                        placeholder="偿还期限（月）" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group  ">
                            <label for="amount" class="col-sm-2 asterisk control-label">偿还数额</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                                    <input required="1" step=".01" type="number" id="amount" name="installments[{{$installment->id}}][amount]" value="{{$installment->amount}}"
                                        class="form-control installments amount" placeholder="偿还数额" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="form-group  ">
                            <label for="file_url" class="col-sm-2 control-label">文件上载</label>
                            <div class="col-sm-8">
                                <div class="file-input">
                                    <div class="col-sm-8">
                                        <input type="file" class="installments file_url no-{{$installment->id}}"
                                            name="installments[{{$installment->id}}][file_url]" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="installments[{{$installment->id}}][id]" value="{{$installment->id}}" class="installments id">
                        <input type="hidden" name="installments[{{$installment->id}}][_remove_]" value="0"
                            class="installments _remove_ fom-removed">
                        <div class="form-group">
                            <label class="col-sm-2  control-label"></label>
                            <div class="col-sm-8">
                                <div class="edit btn btn-warning btn-sm pull-right">
                                    <i class="fa fa-pencil">&nbsp;</i>更改
                                </div>
                            </div>
                        </div>    
                        <div class="form-group">
                            <label class="col-sm-2  control-label"></label>
                            <div class="col-sm-8">
                                <div class="remove btn btn-danger btn-sm pull-right">
                                    <i class="fa fa-trash">&nbsp;</i>删除
                                </div>
                            </div>
                        </div>
                        <hr>
                    </div>
                @endforeach
            </div>
            <template class="installments-tpl">
                <div class="has-many-installments-form fields-group">
                    <div class="form-group  ">
                        <label for="payable_date" class="col-sm-2 asterisk control-label">偿还日期</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span>
                                <input required="1" style="width: 110px" type="text" id="payable_date"
                                    name="installments[new___LA_KEY__][payable_date]" value=""
                                    class="form-control installments payable_date" placeholder="偿还日期">
                            </div>
                        </div>
                    </div>
                    <div class="form-group  ">
                        <label for="total_paid_months" class="col-sm-2 asterisk control-label">偿还期限（月）</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                                <input required="1" type="number" id="total_paid_months"
                                    name="installments[new___LA_KEY__][total_paid_months]" value=""
                                    class="form-control installments total_paid_months"
                                    placeholder="偿还期限（月）">
                            </div>
                        </div>
                    </div>
                    <div class="form-group  ">
                        <label for="amount" class="col-sm-2 asterisk control-label">偿还数额</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                                <input required="1" step=".01" type="number" id="amount" name="installments[new___LA_KEY__][amount]" value=""
                                    class="form-control installments amount" placeholder="偿还数额">
                            </div>
                        </div>
                    </div>
                    <div class="form-group  ">
                        <label for="file_url" class="col-sm-2 asterisk control-label">文件上载</label>
                        <div class="col-sm-8">
                            <input type="file" class="installments file_url"
                                name="installments[new___LA_KEY__][file_url]" required="1">
                        </div>
                    </div>
                    <input type="hidden" name="installments[new___LA_KEY__][id]" value=""
                        class="installments id"><input type="hidden" name="installments[new___LA_KEY__][_remove_]"
                        value="0" class="installments _remove_ fom-removed">
                    <div class="form-group">
                        <label class="col-sm-2  control-label"></label>
                        <div class="col-sm-8">
                            <div class="remove btn btn-warning btn-sm pull-right"><i
                                    class="fa fa-trash"></i>&nbsp;删除</div>
                        </div>
                    </div>
                    <hr>
                </div>
            </template>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            {{ csrf_field() }}
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <input type="hidden" name="id" value="{{$application->id}}">
                <div class="btn-group pull-right">
                    <button type="submit" class="btn btn-primary">更改</button>
                </div>
                <div class="btn-group pull-left">
                    <button type="reset" class="btn btn-warning">重置</button>
                </div>
            </div>
        </div>


    </form>
</div>
<input type="hidden" name="installments[10]" value="22" class="installments id">
<script>
    $(document).ready(function() {
        $(".loanee_company_state").select2({
            "allowClear": true,
            "placeholder": {
                "id": "",
                "text": "公司地区"
            }
        });

        $(".category_id").select2({
            "allowClear": true,
            "placeholder": {
                "id": "",
                "text": "服务性质"
            }
        });
        $(".loan_status").select2({
            "allowClear": true,
            "placeholder": {
                "id": "",
                "text": "借款状态"
            }
        });

        $('.new').iCheck({
            radioClass: 'iradio_minimal-blue'
        });
        $('.online_commerce').iCheck({
            radioClass: 'iradio_minimal-blue'
        });

        var imageUrl = "https://segiloan.s3.ap-southeast-1.amazonaws.com/{{ $application->file_url }}";

        $("input.file_url_a").fileinput({
            "initialPreview": imageUrl,
            "initialPreviewConfig": [{
                "caption": "{{ $application->file_url }}",
                "key": 0,
                "type": "image",
                "downloadUrl": imageUrl
            }],
            "overwriteInitial": true,
            "initialPreviewAsData": true,
            "msgPlaceholder": "文件上载",
            "browseLabel": "浏览",
            "cancelLabel": "取消",
            "showRemove": false,
            "showUpload": false,
            "showCancel": false,
            "dropZoneEnabled": false,
            "deleteExtraData": {
                "file_url": "_file_del_",
                "_file_del_": "",
                "_token": "fORlKYvrZM7j5KBGkAs2gtLBkQknrXGpWB5T4Mk5",
                "_method": "PUT"
            },
            "deleteUrl": "http:\/\/localhost:8000\/admin\/",
            "fileActionSettings": {
                "showRemove": false,
                "showDrag": false
            }
        });

        var index = 0;
        $('#has-many-installments').off('click', '.add').on('click', '.add', function() {

            var tpl = $('template.installments-tpl');

            index++;

            var template = tpl.html().replace(/__LA_KEY__/g, index);
            $('.has-many-installments-forms').append(template);
            $('.installments.payable_date').parent().datetimepicker({
                "format": "YYYY-MM-DD",
                "locale": "en",
                "allowInputToggle": true
            });
            $("input.installments.file_url").fileinput({
                "overwriteInitial": true,
                "initialPreviewAsData": true,
                "msgPlaceholder": "文件上载",
                "browseLabel": "浏览",
                "cancelLabel": "取消",
                "showRemove": false,
                "showUpload": false,
                "showCancel": false,
                "dropZoneEnabled": false,
                "deleteExtraData": {
                    "file_url": "_file_del_",
                    "_file_del_": "",
                    "_token": "Zb6FTQILm0O23roGxD77e0s2fex9nuYQl6yaMa3I",
                    "_method": "PUT"
                },
                "deleteUrl": "http:\/\/localhost:8000\/admin\/application\/2",
                "fileActionSettings": {
                    "showRemove": false,
                    "showDrag": false
                }
            });
            return false;
        });

        $('#has-many-installments').off('click', '.edit').on('click', '.edit', function() {
            $(this).closest('.has-many-installments-form').find('input').removeAttr('readonly');
            $(this).closest('.has-many-installments-form').find('input').removeAttr('disabled');
            $(this).closest('.has-many-installments-form').find('div[tabindex]').removeAttr('disabled').removeClass('disabled');
            return false;
        });

        $('#has-many-installments').off('click', '.remove').on('click', '.remove', function() {
            $(this).closest('.has-many-installments-form').find('input').removeAttr('required');
            $(this).closest('.has-many-installments-form').hide();
            $(this).closest('.has-many-installments-form').find('.fom-removed').val(1);
            return false;
        });

        $('.installments.payable_date').parent().datetimepicker({
            "format": "YYYY-MM-DD",
            "locale": "en",
            "allowInputToggle": true
        });
 
        @foreach($application->installments as $key => $installment)
            $("input.installments.file_url.no-{{$installment->id}}").fileinput({
                "initialPreview": "https://segiloan.s3.ap-southeast-1.amazonaws.com/{{$installment->file_url}}",
                "initialPreviewConfig": [{
                    "caption": "{{$installment->file_url}}",
                    "key": 0,
                    "type": "image",
                    "downloadUrl": "https://segiloan.s3.ap-southeast-1.amazonaws.com/{{$installment->file_url}}"
                }],
                "overwriteInitial": true,
                "initialPreviewAsData": true,
                "msgPlaceholder": "文件上载",
                "browseLabel": "浏览",
                "cancelLabel": "取消",
                "showRemove": false,
                "showUpload": false,
                "showCancel": false,
                "dropZoneEnabled": false,
                "deleteExtraData": {
                    "file_url": "_file_del_",
                    "_file_del_": "",
                    "_token": "Zb6FTQILm0O23roGxD77e0s2fex9nuYQl6yaMa3I",
                    "_method": "PUT"
                },
                "deleteUrl": "http:\/\/localhost:8000\/admin\/application\/2",
                "fileActionSettings": {
                    "showRemove": false,
                    "showDrag": false
                }
            });
        @endforeach

    });
</script>
