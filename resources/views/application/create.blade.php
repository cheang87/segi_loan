@if($errors->any())
   @foreach($errors->all() as $error)
      <div class="alert alert-danger alert-dismissible fade in" role="alert">
         <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
         <strong>{{$error}}</strong>
      </div>
   @endforeach
@endif

<div class="box box-info">
    <div class="box-header with-border">
       <h3 class="box-title">创建</h3>
       <div class="box-tools">
          <div class="btn-group pull-right" style="margin-right: 5px">
             <a href="/admin/application" class="btn btn-sm btn-default" title="List"><i class="fa fa-list"></i><span class="hidden-xs">&nbsp;列表</span></a>
          </div>
       </div>
    </div>
    <form action="/admin/application/savePost" method="post" class="form-horizontal model-form-61eeae3022506" enctype="multipart/form-data">
       <div class="box-body">
          <div class="fields-group">
             <div class="col-md-12">
                <div class="form-group  ">
                   <label for="new" class="col-sm-2  control-label">商家</label>
                   <div class="col-sm-8">
                      <span class="icheck">
                         <label class="radio-inline">
                            <div class="iradio_minimal-blue custom-iradio" style="position: relative;">
                                <input type="radio" name="new" value="0" checked="" class="minimal new" style="position: absolute; opacity: 0;">
                            </div>
                            &nbsp;新的&nbsp;&nbsp;
                         </label>
                      </span>
                      <span class="icheck">
                         <label class="radio-inline">
                            <div class="iradio_minimal-blue" style="position: relative;">
                                <input type="radio" name="new" value="1"  class="minimal new" style="position: absolute; opacity: 0;">
                            </div>
                            &nbsp;现有的&nbsp;&nbsp;
                         </label>
                      </span>
                   </div>
                </div>
                <div class="form-group  ">
                   <label for="online_commerce" class="col-sm-2  control-label">网店</label>
                   <div class="col-sm-8">
                      <span class="icheck">
                         <label class="radio-inline">
                           <div class="iradio_minimal-blue" style="position: relative;">
                              <input type="radio" name="online_commerce" value="0" class="minimal online_commerce" checked="" style="position: absolute; opacity: 0;">
                           </div>
                            &nbsp;不是的&nbsp;&nbsp;
                         </label>
                      </span>
                      <span class="icheck">
                         <label class="radio-inline">
                           <div class="iradio_minimal-blue" style="position: relative;">
                              <input type="radio" name="online_commerce" value="1" class="minimal online_commerce" style="position: absolute; opacity: 0;">
                           </div>
                            &nbsp;是的&nbsp;&nbsp;
                         </label>
                      </span>
                   </div>
                </div>
                <div class="form-group  ">
                   <label for="loanee_Id" class="col-sm-2 asterisk control-label">借贷者ID</label>
                   <div class="col-sm-8">
                      <div class="input-group">
                         <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                         <input required="1" type="text" id="loanee_Id" name="loanee_Id" value="" class="form-control loanee_Id" placeholder="借贷者ID">
                      </div>
                   </div>
                </div>
                <div class="form-group  ">
                   <label for="loanee_name" class="col-sm-2 asterisk control-label">借贷者名称</label>
                   <div class="col-sm-8">
                      <div class="input-group">
                         <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                         <input required="1" type="text" id="loanee_name" name="loanee_name" value="" class="form-control loanee_name" placeholder="借贷者名称">
                      </div>
                   </div>
                </div>
                <div class="form-group  ">
                   <label for="loanee_company" class="col-sm-2 asterisk control-label">公司名称</label>
                   <div class="col-sm-8">
                      <div class="input-group">
                         <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                         <input required="1" type="text" id="loanee_company" name="loanee_company" value="" class="form-control loanee_company" placeholder="公司名称">
                      </div>
                   </div>
                </div>
                <div class="form-group  ">
                   <label for="loanee_company_address" class="col-sm-2 asterisk control-label">公司地址</label>
                   <div class="col-sm-8">
                      <div class="input-group">
                         <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                         <input required="1" type="text" id="loanee_company_address" name="loanee_company_address" value="" class="form-control loanee_company_address" placeholder="公司地址">
                      </div>
                   </div>
                </div>
                <div class="form-group  ">
                   <label for="loanee_company_registration_no" class="col-sm-2 asterisk control-label">公司注册号码</label>
                   <div class="col-sm-8">
                      <div class="input-group">
                         <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                         <input required="1" type="text" id="loanee_company_registration_no" name="loanee_company_registration_no" value="" class="form-control loanee_company_registration_no" placeholder="公司注册号码">
                      </div>
                   </div>
                </div>
                <div class="form-group  ">
                   <label for="loanee_company_state" class="col-sm-2 asterisk  control-label">公司地区</label>
                   <div class="col-sm-8">
                      <input type="hidden" required="1" name="loanee_company_state">
                      <select class="form-control loanee_company_state select2-hidden-accessible" required="1" style="width: 100%;" name="loanee_company_state" data-value="" tabindex="-1" aria-hidden="true">
                         <option value=""></option>
                         <option value="Kedah">Kedah</option>
                         <option value="Penang">Penang</option>
                         <option value="Johor">Johor</option>
                         <option value="Kelantan">Kelantan</option>
                         <option value="Melaka">Melaka</option>
                         <option value="Negeri Sembilan">Negeri Sembilan</option>
                         <option value="Pahang">Pahang</option>
                         <option value="Perak">Perak</option>
                         <option value="Perlis">Perlis</option>
                         <option value="Selangor">Selangor</option>
                         <option value="Terengganu">Terengganu</option>
                         <option value="Sabah">Sabah</option>
                         <option value="Sarawak">Sarawak</option>
                      </select>
                   </div>
                </div>
                <div class="form-group  ">
                   <label for="loanee_email" class="col-sm-2 asterisk control-label">公司邮件地址</label>
                   <div class="col-sm-8">
                      <div class="input-group">
                         <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                         <input type="email" id="loanee_email" required="1" name="loanee_email" value="" class="form-control loanee_email" placeholder="公司邮件地址">
                      </div>
                   </div>
                </div>
                <div class="form-group  ">
                   <label for="loanee_company_contact" class="col-sm-2 asterisk control-label">公司联络号码</label>
                   <div class="col-sm-8">
                      <div class="input-group">
                         <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                         <input required="1" type="text" id="loanee_company_contact" name="loanee_company_contact" value="" class="form-control loanee_company_contact" placeholder="公司联络号码">
                      </div>
                   </div>
                </div>
                <div class="form-group  ">
                   <label for="category_id" class="col-sm-2 asterisk control-label">服务性质</label>
                   <div class="col-sm-8">
                      <input type="hidden" name="category_id">
                      <select class="form-control category_id select2-hidden-accessible" required="1" style="width: 100%;" name="category_id" data-value="" tabindex="-1" aria-hidden="true">
                        <option value=""></option>
                        @foreach($categories as $category)
                            <option value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                      </select>
                   </div>
                </div>
                <div class="form-group  ">
                   <label for="loan_officer_name" class="col-sm-2 asterisk control-label">放款负责人</label>
                   <div class="col-sm-8">
                      <div class="input-group">
                         <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                         <input required="1" type="text" id="loan_officer_name" name="loan_officer_name" value="" class="form-control loan_officer_name" placeholder="放款负责人">
                      </div>
                   </div>
                </div>
                <div class="form-group  ">
                   <label for="loan_officer_contact" class="col-sm-2 asterisk control-label">放款负责联络</label>
                   <div class="col-sm-8">
                      <div class="input-group">
                         <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                         <input required="1" type="text" id="loan_officer_contact" name="loan_officer_contact" value="" class="form-control loan_officer_contact" placeholder="放款负责联络">
                      </div>
                   </div>
                </div>
                <div class="form-group  ">
                   <label for="loan_officer_email" class="col-sm-2 asterisk control-label">放款负责邮件地址</label>
                   <div class="col-sm-8">
                      <div class="input-group">
                         <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                         <input type="email" required="1" id="loan_officer_email" name="loan_officer_email" value="" class="form-control loan_officer_email" placeholder="放款负责邮件地址">
                      </div>
                   </div>
                </div>
                <div class="form-group  ">
                   <label for="loan_amount" class="col-sm-2 asterisk control-label">借款数额</label>
                   <div class="col-sm-8">
                      <div class="input-group">
                         <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                         <input type="number" step=".01" required="1" id="loan_amount" name="loan_amount" value="" class="form-control loan_amount" placeholder="借款数额">
                      </div>
                   </div>
                </div>
                <div class="form-group  ">
                   <label for="loan_period" class="col-sm-2 asterisk control-label">借贷期限 （月）</label>
                   <div class="col-sm-8">
                      <div class="input-group">
                         <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                         <input type="number" required="1" id="loan_period" name="loan_period" value="" class="form-control loan_period" placeholder="借贷期限 （月）">
                      </div>
                   </div>
                </div>
                <div class="form-group  ">
                   <label for="loan_status" class="col-sm-2 asterisk control-label">借款状态</label>
                   <div class="col-sm-8">
                      <input type="hidden" name="loan_status">
                      <select class="form-control loan_status select2-hidden-accessible" required="1" style="width: 100%;" name="loan_status" data-value="" tabindex="-1" aria-hidden="true">
                         <option value=""></option>
                         <option value="0">冻结</option>
                         <option value="1">运行中</option>
                         <option value="2">完成</option>
                      </select>
                   </div>
                </div>
                <div class="form-group  ">
                   <label for="file_url" class="col-sm-2 asterisk control-label">文件上载</label>
                   <div class="col-sm-8">
                      <div class="file-input">
                         <input class="file_url" name="file" type="file" class="file" required="1">
                      </div>
                   </div>
                </div>
             </div>
          </div>
       </div>
       <!-- /.box-body -->
       <div class="box-footer">
            {{ csrf_field() }}
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="btn-group pull-right">
                    <button type="submit" class="btn btn-primary">提交</button>
                </div>
                <div class="btn-group pull-left">
                    <button type="reset" class="btn btn-warning">重置</button>
                </div>
            </div>
       </div>
    </form>
 </div>
 <script> 
 $(document).ready(function() {
   

    $(".loanee_company_state").select2({
        "allowClear": true,
        "placeholder": {
            "id": "",
            "text": "公司地区"
        }
    });

    $(".category_id").select2({
        "allowClear": true,
        "placeholder": {
            "id": "",
            "text": "服务性质 "
        }
    });
    $(".loan_status").select2({
        "allowClear": true,
        "placeholder": {
            "id": "",
            "text": "借款状态"
        }
    });

    $('.new').iCheck({
        radioClass: 'iradio_minimal-blue'
    });
    $('.online_commerce').iCheck({
        radioClass: 'iradio_minimal-blue'
    });

    $("input.file_url").fileinput({
        "overwriteInitial": true,
        "initialPreviewAsData": true,
        "msgPlaceholder": "文件上载",
        "browseLabel": "浏览",
        "cancelLabel": "取消",
        "showRemove": false,
        "showUpload": false,
        "showCancel": false,
        "dropZoneEnabled": false,
        "deleteExtraData": {
            "file_url": "_file_del_",
            "_file_del_": "",
            "_token": "fORlKYvrZM7j5KBGkAs2gtLBkQknrXGpWB5T4Mk5",
            "_method": "PUT"
        },
        "deleteUrl": "http:\/\/localhost:8000\/admin\/",
        "fileActionSettings": {
            "showRemove": false,
            "showDrag": false
        }
    });
 });
</script>