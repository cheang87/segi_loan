<div class="box grid-box p-8">
    <div class="filter-btn inline-custom">
        <div class="btn-group" style="margin-right: 5px" data-toggle="buttons">
            <label class="btn btn-sm btn-dropbox 61eba83bd0b49-filter-btn" title="Filter">
                <input type="checkbox"><i class="fa fa-filter"></i><span class="hidden-xs">&nbsp;&nbsp;筛选</span>
            </label>
        </div>
    </div>
    @if(Admin::user()->isRole('administrator'))
    <div class="btn-group grid-create-btn inline-custom" style="margin-right: 10px">
        <a href="/admin/application/new" class="btn btn-sm btn-success" title="New">
            <i class="fa fa-plus"></i><span class="hidden-xs">&nbsp;&nbsp;创建</span>
        </a>
    </div>
    @endif
    <div class="box-header with-border filter-box hide" id="filter-box">
        <form action="{{ route('admin.application.index')}}" class="form-horizontal" pjax-container="" method="get">
            <div class="row">
                <div class="col-md-12">
                    <div class="box-body">
                        <div class="fields-group">
                            @if(Admin::user()->isRole('administrator'))
                            <div class="form-group">
                                <label class="col-sm-2 control-label">借贷者ID</label>
                                <div class="col-sm-8">
                                    <div class="input-group input-group-sm">
                                        <div class="input-group-addon">
                                            <i class="fa fa-pencil"></i>
                                        </div>
                                        <input type="text" class="form-control loanee_Id" placeholder="借贷者ID"
                                            name="loanee_Id" value="">
                                    </div>
                                </div>
                            </div>
                            @endif
                            <div class="form-group">
                                <label class="col-sm-2 control-label">服务性质</label>
                                <div class="col-sm-8">
                                    <select class="form-control category_id select2-hidden-accessible"
                                        name="category_id" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                        <option></option>
                                        @foreach ($categories as $category)
                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">放款负责人</label>
                                <div class="col-sm-8">
                                    <div class="input-group input-group-sm">
                                        <div class="input-group-addon">
                                            <i class="fa fa-pencil"></i>
                                        </div>
                                        <input type="text" class="form-control loan_officer_name" placeholder="放款负责人"
                                            name="loan_officer_name" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">借款状态</label>
                                <div class="col-sm-8">
                                    <select class="form-control loan_status select2-hidden-accessible"
                                        name="loan_status" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                        <option></option>
                                        <option value="0">冻结</option>
                                        <option value="1">运行中</option>
                                        <option value="2">完成</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">地区</label>
                                <div class="col-sm-8">
                                    <select class="form-control loanee_company_state select2-hidden-accessible"
                                        name="loanee_company_state" style="width: 100%;" tabindex="-1"
                                        aria-hidden="true">
                                        <option></option>
                                        <option value="Kedah">Kedah</option>
                                        <option value="Penang">Penang</option>
                                        <option value="Johor">Johor</option>
                                        <option value="Kelantan">Kelantan</option>
                                        <option value="Melaka">Melaka</option>
                                        <option value="Negeri Sembilan">Negeri Sembilan</option>
                                        <option value="Pahang">Pahang</option>
                                        <option value="Perak">Perak</option>
                                        <option value="Perlis">Perlis</option>
                                        <option value="Selangor">Selangor</option>
                                        <option value="Terengganu">Terengganu</option>
                                        <option value="Sabah">Sabah</option>
                                        <option value="Sarawak">Sarawak</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <div class="btn-group pull-left">
                                <button class="btn btn-info submit btn-sm"><i
                                        class="fa fa-search"></i>&nbsp;&nbsp;搜索</button>
                            </div>
                            <div class="btn-group pull-left " style="margin-left: 10px;">
                                <a href="http://localhost:8000/admin/application" class="btn btn-default btn-sm"><i
                                        class="fa fa-undo"></i>&nbsp;&nbsp;重新设置</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <table id="example" class="display" style="width:100%">
        <thead>
            <tr>
                <th>id</th>
                <th>借贷者ID</th>
                <th>服务性质</th>
                <th>放款负责人</th>
                <th>借款数额</th>
                <th>借款状态</th>
                <th>还款记录</th>
                <th>尚欠数额</th>
                <th>网店</th>
                <th>其他</th>
            </tr>
        </thead>
        <tbody>
            @foreach($applications as $key => $application)
            <tr>
                <td>{{ $application->id }}</td>
                <td>{{ $application->loanee_Id }}</td>
                <td>{{ $application->category['name'] }} </td>
                <td>{{ $application->loan_officer_name }}</td>
                <td>{{ $application->loan_amount }}</td>
                <td>{{ $application->loan_text_status }}</td>
                <td>{{ $application->last_payment_history }}</td>
                <td>{{ $application->pay_amount }}</td>
                <td>{{ $application->commerce_text }}</td>
                <td>
                    <div class="grid-dropdown-actions dropdown">
                        <a href="#" style="padding: 0 10px;" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-ellipsis-v"></i>
                        </a>
                        <ul class="dropdown-menu">
                            @if(Admin::user()->isRole('administrator'))
                            <li><a href="/admin/application/edit/{{$application->id}}">更改</a></li>
                            @endif
                            <li><a href="/admin/application/show/{{$application->id}}">详情</a></li>
                            @if(Admin::user()->isRole('administrator'))
                            <li><a data-_key="{{$application->id}}" href="javascript:void(0);"
                                    class="grid-row-action-delete">删除</a></li>
                            @endif
                        </ul>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
<script>
    $(document).ready(function() {

        $('#example').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'excel','pdf'
            ],
            oLanguage: {
                "sSearch": "搜索:",
                "sInfo": "从 _START_ 到 _END_ ，总共 _TOTAL_ 条",
                "oPaginate": {
                    "sPrevious": "上一页",
                    "sNext": "下一页"
                }
            }
        });

        var $btn = $('.61eba83bd0b49-filter-btn');
        var $filter = $('#filter-box');
        $btn.click(function(e){
            if ($filter.is(':visible')) {
                $filter.addClass('hide');
            } else {
                $filter.removeClass('hide');
            }
        })

        $(".category_id").select2({
            placeholder: {"id":"","text":"选择"},
            "allowClear":true
        });

        $(".loan_status").select2({
            placeholder: {"id":"","text":"选择"},
            "allowClear":true
        });
        $(".loanee_company_state").select2({
            placeholder: {"id":"","text":"选择"},
            "allowClear":true
        });

        $('#example').on('click','.grid-row-action-delete',function () {
            var id = $(this).data('_key');

            swal({
                title: "确定删除此纪录?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "确定",
                showLoaderOnConfirm: true,
                cancelButtonText: "取消",
                preConfirm: function() {
                    return new Promise(function(resolve) {
                        $.ajax({
                            method: 'post',
                            url: '/admin/application/delete/' + id,
                            data: {
                                _token: $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function (data) {
                                $.pjax.reload('#pjax-container');
                                resolve(data);
                            }
                        });
                    });
                }
            }).then(function(result) {
                var data = result.value;
                console.log(result);
                if (typeof data === 'object') {
                    if (data.status) {
                        swal(data.message, '', 'success');
                    } else {
                        swal(data.message, '', 'error');
                    }
                }
            });

        });
    } );
</script>