<div class="row">
    <div class="col-md-12">
       <div class="box box-info">
          <!-- form start -->
          <div class="form-horizontal">
             <div class="box-body">
                <div class="fields-group">
                   <div class="form-group ">
                      <label class="col-sm-2 control-label">Merchant</label>
                      <div class="col-sm-8">
                         <div class="box box-solid box-default no-margin box-show">
                            <!-- /.box-header -->
                            <div class="box-body">
                               {{$application->merchant_text}}
                            </div>
                            <!-- /.box-body -->
                         </div>
                      </div>
                   </div>
                   <div class="form-group ">
                      <label class="col-sm-2 control-label">Online E commerce</label>
                      <div class="col-sm-8">
                         <div class="box box-solid box-default no-margin box-show">
                            <!-- /.box-header -->
                            <div class="box-body">
                                {{$application->commerce_text}}
                            </div>
                            <!-- /.box-body -->
                         </div>
                      </div>
                   </div>
                   <div class="form-group ">
                      <label class="col-sm-2 control-label">Loanee Id</label>
                      <div class="col-sm-8">
                         <div class="box box-solid box-default no-margin box-show">
                            <!-- /.box-header -->
                            <div class="box-body">
                                {{$application->loanee_Id}}
                            </div>
                            <!-- /.box-body -->
                         </div>
                      </div>
                   </div>
                   <div class="form-group ">
                      <label class="col-sm-2 control-label">Loanee Name</label>
                      <div class="col-sm-8">
                         <div class="box box-solid box-default no-margin box-show">
                            <!-- /.box-header -->
                            <div class="box-body">
                                {{$application->loanee_name}}
                            </div>
                            <!-- /.box-body -->
                         </div>
                      </div>
                   </div>
                   <div class="form-group ">
                      <label class="col-sm-2 control-label">Company Name</label>
                      <div class="col-sm-8">
                         <div class="box box-solid box-default no-margin box-show">
                            <!-- /.box-header -->
                            <div class="box-body">
                                {{$application->loanee_company}}
                            </div>
                            <!-- /.box-body -->
                         </div>
                      </div>
                   </div>
                   <div class="form-group ">
                      <label class="col-sm-2 control-label">Company Registration No</label>
                      <div class="col-sm-8">
                         <div class="box box-solid box-default no-margin box-show">
                            <!-- /.box-header -->
                            <div class="box-body">
                                {{$application->loanee_company_registration_no}}
                            </div>
                            <!-- /.box-body -->
                         </div>
                      </div>
                   </div>
                   <div class="form-group ">
                      <label class="col-sm-2 control-label">Company Registration Address</label>
                      <div class="col-sm-8">
                         <div class="box box-solid box-default no-margin box-show">
                            <!-- /.box-header -->
                            <div class="box-body">
                                {{$application->loanee_company_address}}
                            </div>
                            <!-- /.box-body -->
                         </div>
                      </div>
                   </div>
                   <div class="form-group ">
                      <label class="col-sm-2 control-label">Office Contact</label>
                      <div class="col-sm-8">
                         <div class="box box-solid box-default no-margin box-show">
                            <!-- /.box-header -->
                            <div class="box-body">
                                {{$application->loanee_company_contact}}
                            </div>
                            <!-- /.box-body -->
                         </div>
                      </div>
                   </div>
                   <div class="form-group ">
                      <label class="col-sm-2 control-label">Company Email</label>
                      <div class="col-sm-8">
                         <div class="box box-solid box-default no-margin box-show">
                            <!-- /.box-header -->
                            <div class="box-body">
                                {{$application->loanee_email}}
                            </div>
                            <!-- /.box-body -->
                         </div>
                      </div>
                   </div>
                   <div class="form-group ">
                      <label class="col-sm-2 control-label">Business Category</label>
                      <div class="col-sm-8">
                         <div class="box box-solid box-default no-margin box-show">
                            <!-- /.box-header -->
                            <div class="box-body">
                                {{$application->category['name']}}
                            </div>
                            <!-- /.box-body -->
                         </div>
                      </div>
                   </div>
                   <div class="form-group ">
                      <label class="col-sm-2 control-label">Loan Officer Name</label>
                      <div class="col-sm-8">
                         <div class="box box-solid box-default no-margin box-show">
                            <!-- /.box-header -->
                            <div class="box-body">
                                {{$application->loan_officer_name}}
                            </div>
                            <!-- /.box-body -->
                         </div>
                      </div>
                   </div>
                   <div class="form-group ">
                      <label class="col-sm-2 control-label">Loan officer Contact</label>
                      <div class="col-sm-8">
                         <div class="box box-solid box-default no-margin box-show">
                            <!-- /.box-header -->
                            <div class="box-body">
                                {{$application->loan_officer_contact}}
                            </div>
                            <!-- /.box-body -->
                         </div>
                      </div>
                   </div>
                   <div class="form-group ">
                      <label class="col-sm-2 control-label">Loan officer Email</label>
                      <div class="col-sm-8">
                         <div class="box box-solid box-default no-margin box-show">
                            <!-- /.box-header -->
                            <div class="box-body">
                                {{$application->loan_officer_email}}
                            </div>
                            <!-- /.box-body -->
                         </div>
                      </div>
                   </div>
                   <div class="form-group ">
                      <label class="col-sm-2 control-label">Loan Amount</label>
                      <div class="col-sm-8">
                         <div class="box box-solid box-default no-margin box-show">
                            <!-- /.box-header -->
                            <div class="box-body">
                                {{$application->loan_amount}}
                            </div>
                            <!-- /.box-body -->
                         </div>
                      </div>
                   </div>
                   <div class="form-group ">
                      <label class="col-sm-2 control-label">Loan Period</label>
                      <div class="col-sm-8">
                         <div class="box box-solid box-default no-margin box-show">
                            <!-- /.box-header -->
                            <div class="box-body">
                                {{$application->loan_period}}
                            </div>
                            <!-- /.box-body -->
                         </div>
                      </div>
                   </div>
                   <div class="form-group ">
                      <label class="col-sm-2 control-label">Loan Status</label>
                      <div class="col-sm-8">
                         <div class="box box-solid box-default no-margin box-show">
                            <!-- /.box-header -->
                            <div class="box-body">
                                {{$application->loan_text_status}}
                            </div>
                            <!-- /.box-body -->
                         </div>
                      </div>
                   </div>
                   <div class="form-group ">
                      <label class="col-sm-2 control-label">File url</label>
                      <div class="col-sm-8">
                         <div class="box box-solid box-default no-margin box-show">
                            <!-- /.box-header -->
                            <div class="box-body">
                               <img src="https://segiloan.s3.ap-southeast-1.amazonaws.com/{{ $application->file_url }}">&nbsp;
                            </div>
                            <!-- /.box-body -->
                         </div>
                      </div>
                   </div>
                </div>
             </div>
             <!-- /.box-body -->
          </div>
       </div>
    </div>
    <div class="col-md-12">
       <div class="box grid-box p-10">
           <h2>Installment</h2>
            <table id="example" class="display" style="width:100%">
                <thead>
                    <tr>
                        <th>Pay Date</th>
                        <th>Amount</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($application->installments as $installment)
                        <tr>
                            <td>{{ $installment->payable_date }}</td>
                            <td>{{ $installment->amount }} </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
       </div>
    </div>
 </div>
 <script>
 $(document).ready(function() {
    $('#example').DataTable({
        searching: false,
        ordering:  false
    });
});
</script>