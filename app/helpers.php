<?php

use GuzzleHttp\Client;

if (!function_exists('segiAPICall')) {
    function segiAPICall(){
        $segi = array();
        $client  = new Client();
        $bearer_token = "8948dd26-fe57-4842-94d0-e5825a2ba24f";
        $header = ['Authorization' => 'Bearer ' . $bearer_token];

        $request = $client->request('GET', 'https://api.coincap.io/v2/rates/malaysian-ringgit', ['headers' => $header]);
        $myr = json_decode($request->getBody(), true);

        $request = $client->request('GET', 'https://app.segi.bz/api/downMarket');
        $coinList = json_decode($request->getBody(), true);

        foreach($coinList['data'] as $value){
            if($value['pair']=="SEGI"){
                $segi = $value;
                break;
            }
        }

        $myr_value = ( $segi['price'] / $myr['data']['rateUsd'] ) * 1.1;

        return number_format($myr_value, 4, '.', '');
    }
}

?>