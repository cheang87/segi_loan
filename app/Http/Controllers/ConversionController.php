<?php
namespace App\Http\Controllers;
use App\Http\Response;

class ConversionController extends Controller
{
    public function index(){
        $myr_value = segiAPICall();

        return response()->json([
            'segi' => $myr_value
        ]);
    }
}