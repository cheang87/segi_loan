<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    use HasFactory;

    protected $fillable = [
        'new','online_commerce','loanee_Id','loanee_name','loanee_company',
        'loanee_company_address','loanee_company_registration_no','loanee_company_state',
        'loanee_email','loanee_company_contact','category_id','loan_officer_name','loan_officer_contact',
        'loan_officer_email','loan_amount','loan_period','loan_status','file_url'
    ];

    protected $appends = ['pay_amount','loan_text_status','last_payment_history','merchant_text','commerce_text'];

    public function installments()
    {
        return $this->hasMany(Installment::class,'application_id','id');
    }

    public function category()
    {
        return $this->hasOne(Category::class,'id','category_id');
    }

    public function latestPayment()
    {
        return $this->hasOne(Installment::class)->latestOfMany();
    }

    public function getLastPaymentHistoryAttribute()
    {
        $latest = $this->latestPayment;

        if($latest){
            return 'Last payment date in '.$latest->payable_date.' '.$latest->amount.' Amount';
        }else{
            return '';
        }
    }

    public function getLoanTextStatusAttribute()
    {
        $loanTextStatus = array(
            0 => '冻结',
            1 => '运行中',
            2 => '完成',
        );

        return  $loanTextStatus[$this->loan_status];
    }

    public function getMerchantTextAttribute(){
        $merchantText = array(
            0 => '新的',
            1 => '现有的'
        );

        return  $merchantText[$this->new];
    }

    public function getCommerceTextAttribute(){
        $commerceText = array(
            0 => '不是的',
            1 => '是的'
        );

        return  $commerceText[$this->online_commerce];
    }

    public function getPayAmountAttribute()
    {
        return $this->loan_amount - $this->installments->sum('amount');
    }
}
