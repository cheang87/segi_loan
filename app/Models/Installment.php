<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Installment extends Model
{
    use HasFactory;

    protected $fillable = ['application_id','payable_date','total_paid_months','amount','file_url'];

    public function application()
    {
        return $this->belongsTo(Application::class,'application_id');
    }
}
