<?php

namespace App\Admin\Actions;

use Encore\Admin\Actions\RowAction;
use Illuminate\Database\Eloquent\Model;

class InstallmentUpdate extends RowAction
{
    public $name = 'Update';

    // public function handle(Model $model)
    // {
    //     return $this->response()->success('Success message.')->refresh();
    // }

    public function href()
    {
        return "/admin/application/update/".$this->getKey();
    }

}