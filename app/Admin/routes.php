<?php

use Illuminate\Routing\Router;

Admin::routes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
    'as'            => config('admin.route.prefix') . '.',
], function (Router $router) {

    $router->get('/', 'HomeController@index')->name('home');
    
    $router->resource('categories', CategoryController::class);
    //$router->resource('application', ApplicationController1::class);

    $router->group(['prefix'=> 'application'],function() use ($router){
        $router->get('/','ApplicationController@index')->name('application.index');
        $router->get('/show/{id}','ApplicationController@getShow')->name('application.getShow');
        $router->get('/edit/{id}','ApplicationController@getEdit')->name('application.getEdit');
        $router->get('/new','ApplicationController@newCreate')->name('application.newCreate');
        $router->post('/delete/{id}','ApplicationController@getDestroy')->name('application.getDestroy');
        $router->post('/savePost','ApplicationController@savePost')->name('application.savePost');
        $router->post('/updatePost','ApplicationController@updatePost')->name('application.updatePost');
    });
});


