<?php

/**
 * Laravel-admin - admin builder based on Laravel.
 * @author z-song <https://github.com/z-song>
 *
 * Bootstraper for Admin.
 *
 * Here you can remove builtin form field:
 * Encore\Admin\Form::forget(['map', 'editor']);
 *
 * Or extend custom form field:
 * Encore\Admin\Form::extend('php', PHPEditor::class);
 *
 * Or require js and css assets:
 * Admin::css('/packages/prettydocs/css/styles.css');
 * Admin::js('/packages/prettydocs/js/main.js');
 *
 */
Use Encore\Admin\Admin;

Encore\Admin\Form::forget(['map', 'editor']);
Admin::css('https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css');
Admin::css('https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css');
Admin::css('/css/custom.css');
Admin::js('https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js');
Admin::js('https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js');
Admin::js('https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js');
Admin::js('https://cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js');
// Admin::js('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js');
// Admin::js('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js');
// Admin::js('https://cdn.jsdelivr.net/npm/pdfmake-chinese@0.0.8/vfs_fonts.js');
Admin::js('/js/pdfmake.min.js');
Admin::js('/js/vfs_fonts.js');


