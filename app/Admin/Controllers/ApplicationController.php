<?php

namespace App\Admin\Controllers;

use App\Models\Application;
use App\Models\Installment;
use App\Models\Category;

use App\Http\Controllers\Controller;
use Encore\Admin\Auth\Permission;
use Encore\Admin\Layout\Content;
use Encore\Admin\Facades\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use DB;

class ApplicationController extends Controller
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Application';

    public function index(){

        $where    = [];

        if(!Admin::user()->isRole('administrator')){
            $where['loanee_Id'] = Admin::user()->contact;
        }

        if(request('loanee_Id')){
            $where['loanee_Id'] = request('loanee_Id');
        }

        if(request('category_id')){
            $where['category_id'] = request('category_id');
        }

        if(request('loan_officer_name')){
            $where['loan_officer_name'] = request('loan_officer_name');
        }

        if(request('loanee_company_state')){
            $where['loanee_company_state'] = request('loanee_company_state');
        }

        $applications = Application::where(function($query) use ($where){
            foreach ($where as $key => $value) {
                $query = $query->where($key, $value);
            }
            return $query;
        })->get();
        
        $categories = Category::all();

        return Admin::content(function (Content $content) use ($applications, $categories) {

            $content->header('主页');
            $content->breadcrumb(
                ['text' => '主页', 'url' => '/admin/application', 'no-pjax' => 1],
            );

            $content->body(view('application.index', compact('applications','categories')));
        });


    }

    public function getShow($id){

        $application = Application::find($id);

        return Admin::content(function (Content $content) use ($application) {
            return $content->body(view('application.show', compact('application')));
        });
    }

    public function newCreate(){
        $categories = Category::all();
        return Admin::content(function (Content $content) use ($categories) {
            return $content->body(view('application.create', compact('categories')));
        });
    }

    public function getEdit($id){
        $application = Application::find($id);
        $categories = Category::all();

        return Admin::content(function (Content $content) use ($application,$categories) {
            return $content->body(view('application.edit', compact('application','categories')));
        });
    }

    public function getDestroy($id){
        $application = Application::find($id);

        if($application->installments->count() > 0 ){
            return Response::json(array(
                "status" => false,
                "result" => "'",
                "message" => "Record cannot delete due to installment record in installment table"
            ));
        }else{
            
            $application->delete();

            return Response::json(array(
                "status" => true,
                "result" => $application,
                "message" => "Record has been deleted"
            ));
        }        
    }

    public function savePost(Request $request){

        $request->validate([
            'new' => 'required',
            'online_commerce' => 'required',
            'loanee_Id' => 'required',
            'loanee_name' => 'required',
            'loanee_company' => 'required',
            'loanee_company_address' => 'required',
            'loanee_company_registration_no' => 'required',
            'loanee_company_state' => 'required',
            'loanee_email' => 'required',
            'loanee_company_contact' => 'required',
            'category_id' => 'required',
            'loan_officer_name' => 'required',
            'loan_officer_contact' => 'required',
            'loan_officer_email' => 'required',
            'loan_amount' => 'required',
            'loan_period' => 'required',
            'loan_status' => 'required',
            'file' => 'required'
        ]);

        $file = $request->file('file');

        $name = time() . $file->getClientOriginalName();
        $filePath = 'images/' . $name;

        $uploaded = Storage::disk(config('admin.upload.disk'))->put($filePath, file_get_contents($file));

        if(!$uploaded){
            return Redirect::back()->withErrors(['Enable Upload Image to S3']);
        }

        $application = Application::create([
            'new' => $request->new,
            'online_commerce' => $request->online_commerce,
            'loanee_Id' => $request->loanee_Id,
            'loanee_name' => $request->loanee_name,
            'loanee_company' => $request->loanee_company,
            'loanee_company_address' => $request->loanee_company_address,
            'loanee_company_registration_no' => $request->loanee_company_registration_no,
            'loanee_company_state' => $request->loanee_company_state,
            'loanee_email' => $request->loanee_email,
            'loanee_company_contact' => $request->loanee_company_contact,
            'category_id' => $request->category_id,
            'loan_officer_name' => $request->loan_officer_name,
            'loan_officer_contact' => $request->loan_officer_contact,
            'loan_officer_email' => $request->loan_officer_email,
            'loan_amount' => $request->loan_amount,
            'loan_period' => $request->loan_period,
            'loan_status' => $request->loan_status,
            'file_url' => $filePath
        ]);

        return redirect()->route('admin.application.index');
        
    }

    public function updatePost(Request $request){

        $request->validate([
            'new' => 'required',
            'online_commerce' => 'required',
            'loanee_Id' => 'required',
            'loanee_name' => 'required',
            'loanee_company' => 'required',
            'loanee_company_address' => 'required',
            'loanee_company_registration_no' => 'required',
            'loanee_company_state' => 'required',
            'loanee_email' => 'required',
            'loanee_company_contact' => 'required',
            'category_id' => 'required',
            'loan_officer_name' => 'required',
            'loan_officer_contact' => 'required',
            'loan_officer_email' => 'required',
            'loan_amount' => 'required',
            'loan_period' => 'required',
            'loan_status' => 'required'
        ]);

        $file = $request->file('file');

        $updateArray = array(
            'new' => $request->new,
            'online_commerce' => $request->online_commerce,
            'loanee_Id' => $request->loanee_Id,
            'loanee_name' => $request->loanee_name,
            'loanee_company' => $request->loanee_company,
            'loanee_company_address' => $request->loanee_company_address,
            'loanee_company_registration_no' => $request->loanee_company_registration_no,
            'loanee_company_state' => $request->loanee_company_state,
            'loanee_email' => $request->loanee_email,
            'loanee_company_contact' => $request->loanee_company_contact,
            'category_id' => $request->category_id,
            'loan_officer_name' => $request->loan_officer_name,
            'loan_officer_contact' => $request->loan_officer_contact,
            'loan_officer_email' => $request->loan_officer_email,
            'loan_amount' => $request->loan_amount,
            'loan_period' => $request->loan_period,
            'loan_status' => $request->loan_status
        );

        if($file){
            $name = time() . $file->getClientOriginalName();
            $filePath = 'images/' . $name;

            $uploaded = Storage::disk(config('admin.upload.disk'))->put($filePath, file_get_contents($file));

            if(!$uploaded){
                return Redirect::back()->withErrors(['Enable Upload Image to S3']);
            }

            $updateArray['file_url'] = $filePath;

        }

        try{
            DB::beginTransaction();
            $application = Application::where('id',$request->id)->update($updateArray);
            if($request->installments && count($request->installments) > 0){
                
                foreach($request->installments as $key => $installment){
                    if(strpos($key, 'new_') === 0){
                        if($installment['_remove_'] == 0){
                            
                            $name = time() . $installment['file_url']->getClientOriginalName();
                            $filePath = 'images/' . $name;

                            $uploaded = Storage::disk(config('admin.upload.disk'))->put($filePath, file_get_contents($installment['file_url']));

                            if(!$uploaded){
                                return Redirect::back()->withErrors(['Enable Upload Image to S3']);
                            }
    
                            Installment::create([
                                'application_id' => $request->id,
                                'payable_date' => $installment['payable_date'],
                                'total_paid_months' => $installment['total_paid_months'],
                                'amount' => $installment['amount'],
                                'file_url' => $filePath
                            ]);
                        }

                    }else{
                        if($installment['_remove_'] == 0){

                            $updateInstallment =  array(
                                'application_id' => $request->id,
                                'payable_date' => $installment['payable_date'],
                                'total_paid_months' => $installment['total_paid_months'],
                                'amount' => $installment['amount']
                            );

                            if(array_key_exists('file_url', $installment)){
                                $name = time() . $installment['file_url']->getClientOriginalName();
                                $filePath = 'images/' . $name;
                        
                                $uploaded = Storage::disk(config('admin.upload.disk'))->put($filePath, file_get_contents($installment['file_url']));
                        
                                if(!$uploaded){
                                    return Redirect::back()->withErrors(['Enable Upload Image to S3']);
                                }

                                $updateInstallment['file_url'] = $filePath;
                            }

                            Installment::where('id',$key)->update($updateInstallment);

                        }else{
                            Installment::where('id',$key)->delete();
                        }
                    }
                }
            }
            DB::commit();

            return redirect()->route('admin.application.index');

        }catch(\Exception $exception){
            dd($exception);
        }
    }
}
