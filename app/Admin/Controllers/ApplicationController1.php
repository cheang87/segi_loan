<?php

namespace App\Admin\Controllers;

use App\Models\Application;
use App\Models\Category;

use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Auth\Permission;
use Encore\Admin\Layout\Content;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Show;
use Encore\Admin\Facades\Admin;
use App\Admin\Actions\InstallmentUpdate;

class ApplicationController1 extends AdminController
//class ApplicationController extends Controller
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Application';

    // public function updateInstallment(Content $content){
    //     //dd(request('id'));

    //     $form = new Form(new Application());
    //     $form->decimal('loan_amount', __('Loan amount'));
    //     $form->number('loan_period', __('Loan period'));
    //     $form->number('loan_status', __('Loan status'));
    //     $form->text('file_url', __('File url'));

    //     $content->body('hello world');
    //     return $content->body($form);
    // }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {

        $grid = new Grid(new Application());

        //$grid->model()->where('new',1);
        
        $grid->column('loanee_Id', __('Loanee Id'));
        $grid->column('category_id', __('Business Category'))->using(
            Category::get()->pluck('name', 'id')->toArray()
        );

        $grid->column('loan_officer_name', __('Loan officer name'));
        $grid->column('loanee_company_state', __('State'));
        $grid->column('loan_amount', __('Loan amount'));
        $grid->column('loan_status', __('Loan status'))->using([
            0 => 'Freeze',
            1 => 'In Progress',
            2 => 'Completed',
        ]);;

        $grid->column('installments', __('Last Payment History'))->display(function ($installments){
            $count = count($installments)-1;

            if($count > 0){
                return 'Last payment date in '.$installments[$count]['payable_date'].' '.$installments[$count]['amount'].' Amount';
            }else{
                return '';
            }
            
        });

        $grid->column('pay_amount', __('Paid Amount'));

        $grid->column('online_commerce', __('Online commerce'))->using([0 => 'No', 1 => 'Yes']);

        if(!Admin::user()->isRole('administrator')){
            $grid->actions(function ($actions) {
                $actions->disableEdit();
                $actions->disableDelete();
            });

            $grid->disableCreateButton();
        }

        $grid->filter(function($filter){

            // Remove the default id filter
            $filter->disableIdFilter();
        
            // Add a column filter
            $filter->like('loanee_Id', 'loanee Id');
            $filter->equal('category_id', 'Business Category')->select(Category::all()->pluck('name', 'id'));
            $filter->like('loan_officer_name', 'Loan officer name');

            $filter->equal('loan_status', __('Loan status'))->select([
                0 => 'Freeze',
                1 => 'In Progress',
                2 => 'Completed',
            ]);

            $filter->equal('loanee_company_state', __('State'))->select([
                'Kedah' => 'Kedah',
                'Penang' => 'Penang',
                'Johor' => 'Johor',
                'Kelantan' => 'Kelantan',
                'Melaka' => 'Melaka',
                'Negeri Sembilan' => 'Negeri Sembilan',
                'Pahang' => 'Pahang',
                'Perak' => 'Perak',
                'Perlis' => 'Perlis',
                'Selangor' => 'Selangor',
                'Terengganu' => 'Terengganu',
                'Sabah' => 'Sabah',
                'Sarawak' => 'Sarawak',
            ]);
        
        });

        // $grid->disableColumnSelector();
        // $grid->disableRowSelector();

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(Application::findOrFail($id));

        if(!Admin::user()->isRole('administrator')){
            $show->panel()->tools(function ($tools) {
                $tools->disableEdit();
                $tools->disableDelete();
            });;
        }

        $show->field('new', __('Merchant'))->using([0 => 'New', 1 => 'Existing']);
        $show->field('online_commerce', __('Online E commerce'))->using([0 => 'No', 1 => 'Yes']);
        $show->field('loanee_Id', __('Loanee Id'));
        $show->field('loanee_name', __('Loanee Name'));

        $show->field('loanee_company', __('Company Name'));
        $show->field('loanee_company_registration_no', __('Company Registration No'));
        $show->field('loanee_company_address', __('Company Registration Address'));
        $show->field('loanee_company_contact', __('Office Contact'));
        $show->field('loanee_email', __('Company Email'));

        $show->field('category_id', __('Business Category'))->using(
            Category::get()->pluck('name', 'id')->toArray()
        );

        $show->field('loan_officer_name', __('Loan Officer Name'));
        $show->field('loan_officer_contact', __('Loan officer Contact'));
        $show->field('loan_officer_email', __('Loan officer Email'));
        $show->field('loan_amount', __('Loan Amount'));
        $show->field('loan_period', __('Loan Period'));
        $show->field('loan_status', __('Loan Status'))->using([0 => 'Freeze', 1 => 'In Progress', 2 => 'Completed']);;

        $show->file_url()->unescape()->as(function ($file_url) {
            return "<img src='".config('filesystems.disks.s3.url').$file_url."' />";
        });

        $show->installments('installments', function ($installments) {
            $installments->disableCreateButton();
            $installments->disableFilter();
            $installments->disableExport();
            $installments->disableActions();
            $installments->disableRowSelector();
            $installments->disableColumnSelector();

            $installments->payable_date('Pay Date');
            $installments->amount();
        });

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new Application());

        if($form->isCreating()){

            $form->radio('new', __('Merchant'))->options(['0' => 'New', '1'=> 'Existing'])->default('0');
            $form->radio('online_commerce', __('Online E commerce'))->options(['0' => 'No', '1'=> 'Yes'])->default('0');

            $form->text('loanee_Id', __('Loanee Id'))->required();
            $form->text('loanee_name', __('Loanee Name'))->required();

            $form->text('loanee_company', __('Company Name'))->required();
            $form->text('loanee_company_address', __('Company Registration Address'))->required();
            $form->text('loanee_company_registration_no', __('Company Registration No'))->required();

            $form->select('loanee_company_state',__('Company Registration State'))->options([
                'Kedah' => 'Kedah',
                'Penang' => 'Penang',
                'Johor' => 'Johor',
                'Kelantan' => 'Kelantan',
                'Melaka' => 'Melaka',
                'Negeri Sembilan' => 'Negeri Sembilan',
                'Pahang' => 'Pahang',
                'Perak' => 'Perak',
                'Perlis' => 'Perlis',
                'Selangor' => 'Selangor',
                'Terengganu' => 'Terengganu',
                'Sabah' => 'Sabah',
                'Sarawak' => 'Sarawak',
            ]);

            $form->text('loanee_email', __('Company Email'))->rules('required|email');
            $form->text('loanee_company_contact', __('Office Contact'))->required();

            $form->select('category_id', __('Business Category'))->options(
                Category::all()->pluck('name', 'id')
            );

            $form->text('loan_officer_name', __('Loan Officer Name'))->required();
            $form->text('loan_officer_contact', __('Loan Officer Contact'))->required();
            $form->text('loan_officer_email', __('Loan Officer Email'))->rules('required|email');

            $form->text('loan_amount', __('Loan amount'))->rules('required|Numeric');
            $form->text('loan_period', __('Loan period'))->rules('required|integer');

            $form->select('loan_status',__('Loan status'))->options([
                0 => 'Freeze',
                1 => 'In Progress',
                2 => 'Completed',
            ]);

            $form->image('file_url',__('Document Upload'));

        }else{

            $form->radio('new', __('Merchant'))->options(['0' => 'New', '1'=> 'Existing'])->default('0');
            $form->radio('online_commerce', __('Online E commerce'))->options(['0' => 'No', '1'=> 'Yes'])->default('0');

            $form->text('loanee_Id', __('Loanee Id'))->required();
            $form->text('loanee_name', __('Loanee Name'))->required();

            $form->text('loanee_company', __('Company Name'))->required();
            $form->text('loanee_company_address', __('Company Registration Address'))->required();
            $form->text('loanee_company_registration_no', __('Company Registration No'))->required();

            $form->select('loanee_company_state',__('Company Registration State'))->options([
                'Kedah' => 'Kedah',
                'Penang' => 'Penang',
                'Johor' => 'Johor',
                'Kelantan' => 'Kelantan',
                'Melaka' => 'Melaka',
                'Negeri Sembilan' => 'Negeri Sembilan',
                'Pahang' => 'Pahang',
                'Perak' => 'Perak',
                'Perlis' => 'Perlis',
                'Selangor' => 'Selangor',
                'Terengganu' => 'Terengganu',
                'Sabah' => 'Sabah',
                'Sarawak' => 'Sarawak',
            ]);

            $form->text('loanee_email', __('Company Email'))->rules('required|email');
            $form->text('loanee_company_contact', __('Office Contact'))->required();

            $form->select('category_id', __('Business Category'))->options(
                Category::all()->pluck('name', 'id')
            );

            $form->text('loan_officer_name', __('Loan Officer Name'))->required();
            $form->text('loan_officer_contact', __('Loan Officer Contact'))->required();
            $form->text('loan_officer_email', __('Loan Officer Email'))->rules('required|email');

            $form->text('loan_amount', __('Loan amount'))->rules('required|Numeric');
            $form->text('loan_period', __('Loan period'))->rules('required|integer');

            $form->select('loan_status',__('Loan status'))->options([
                0 => 'Freeze',
                1 => 'In Progress',
                2 => 'Completed',
            ]);

            $form->image('file_url',__('Document Upload'))->required();


            $form->hasMany('installments', function (Form\NestedForm $form) {
                $form->date('payable_date')->required();
                $form->text('total_paid_months')->rules('required|integer');
                $form->text('amount')->rules('required|Numeric');
                $form->image('file_url')->required();
            });
        }

        return $form;
    }
}
